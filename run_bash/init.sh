export fast=1

export run_example=0

export verbose=1

export n_workers=1

export accelerated=1

export M=5

export num_of_rabin_pairs=2

export num_of_live_edges=-1  

export parallelised=1
