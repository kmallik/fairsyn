#!/bin/bash


source ./init.sh

for((file_num=1;file_num<=1;file_num++));
do

    for((seed=1;seed<=1;seed++));
    
    do
	    timeout 7210 ./../bin/main_fairsyn $fast $run_example $verbose $n_workers $accelerated $M $num_of_rabin_pairs $num_of_live_edges $seed info_${parallelised}_${accelerated}.txt controller_${parallelised}_${accelerated}_${file_num}.txt ../bcg/output/${file_num}.txt
        echo "DONE" $file_num $seed
    done

done
