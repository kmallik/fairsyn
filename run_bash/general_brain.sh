#!/bin/bash
cd ../build
make
cd ../run_bash
for ((i = 1; i <= 3; i++)); do
  for ((j = 1; j <= 3; j++)); do
    echo ""
    echo "#################################"
    echo "START general_queue cudd" $i $j
    ./../build/bin/general_queue $i $j 0 10 0
    echo "DONE general_queue cudd" $i $j
    echo ""
    echo "#################################"
    echo "START general_queue sylvan" $i $j
    ./../build/bin/general_queue $i $j 0 10 3
    echo "DONE general_queue sylvan" $i $j
  done
done
