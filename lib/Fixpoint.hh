/** @file Fixpoint.hh
 */

#ifndef FIXPOINT_HH_
#define FIXPOINT_HH_

#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <map>
#include <stdexcept>
#include <vector>

#include "Arena.hh"
#include "Helper.hh"
#include "RabinAutomaton.hh"
#include "lib/BaseFixpoint.hh"
#include "utils/TicToc.hh"

namespace fairsyn {

    /**
     * @brief class for the fixed point computation for the Rabin specification
     *
     * provides the fixed point computation for the Rabin specification
     * on finite transition systems with the edge fairness condition
     */

    template <class UBDD>
    class Fixpoint : public genie::BaseFixpoint<UBDD> {
    public:
        using BaseFixpoint<UBDD>::base_;
        using BaseFixpoint<UBDD>::str_;
        using BaseFixpoint<UBDD>::nodes_;
        using BaseFixpoint<UBDD>::sys_nodes_;
        using BaseFixpoint<UBDD>::env_nodes_;
        using BaseFixpoint<UBDD>::tr_;
        using BaseFixpoint<UBDD>::live_;
        using BaseFixpoint<UBDD>::cubeOther_;
        using BaseFixpoint<UBDD>::preVars_;
        using BaseFixpoint<UBDD>::postVars_;
        using BaseFixpoint<UBDD>::cubePost_;
        using BaseFixpoint<UBDD>::RabinPairs_;
        using BaseFixpoint<UBDD>::printTabs;

        /**
         * @brief constructor
         * @details The rabin pairs are given as a vector of the pairs (Gi,Ri) for the specification (GF Gi & FG !Ri)
         */
        Fixpoint(UBDD &base,
                   const char *str,
                   std::vector<size_t> &nodes,
                   std::vector<size_t> &sys_nodes,
                   std::vector<size_t> &env_nodes,
                   std::vector<std::vector<size_t>> &transitions,
                   std::vector<std::vector<size_t>> &live_edges,
                   std::vector<std::array<std::vector<size_t>, 2>> &RabinPairs, std::set<size_t> &all_variables) {
            // init values from Arena
            base_ = base;
            Arena<UBDD> arena = Arena<UBDD>(base_, all_variables, nodes, sys_nodes, env_nodes, transitions, live_edges);
            str_ = str;
            nodes_ = arena.nodes_;
            sys_nodes_ = arena.sys_nodes_;
            env_nodes_ = arena.env_nodes_;
            preVars_ = arena.preVars_;
            postVars_ = arena.postVars_;
            tr_ = arena.tr_;
            live_ = arena.live_;
            cubePost_ = base_.cube(helper::toUBDD<UBDD>(base_, helper::sort(postVars_)));
            cubeOther_ = base_.one();
            for (size_t i = 0; i < RabinPairs.size(); ++i) {
                rabin_pair_<UBDD> p;
                p.rabin_index_ = i;
                p.G_ = helper::setToBdd<UBDD>(base_, RabinPairs[i][0], preVars_, preVars_.size());
                p.nR_ = (!(helper::setToBdd<UBDD>(base_, RabinPairs[i][1], preVars_, preVars_.size()))) & nodes_;
                RabinPairs_.push_back(p);
            }

            adjust_for_approximation_type();
        }

        Fixpoint(UBDD &base,
                   const char *str,
                   const Arena<UBDD> *other,
                   const RabinAutomaton<UBDD> *rabin,
                   const std::vector<size_t> &other_variables = std::vector<size_t>()) {
            base_ = base;
            str_ = str;
            /* the set of nodes of this automaton is the product (computed using BDD product) state space */
            nodes_ = other->nodes_ * rabin->stateSpace_;
            /* the system nodes in the product are those nodes whose respective component in "other" belongs to the system nodes in "other" */
            sys_nodes_ = other->sys_nodes_;
            /* similarly, the environment nodes */
            env_nodes_ = other->env_nodes_;
            /* the set of variables is the union of variables of the arena and the rabin automaton */
            preVars_ = other->preVars_;
            for (auto i = rabin->stateVars_.begin(); i != rabin->stateVars_.end(); ++i) {
                preVars_.push_back(*i);
            }
            postVars_ = other->postVars_;
            for (auto i = rabin->postStateVars_.begin(); i != rabin->postStateVars_.end(); ++i) {
                postVars_.push_back(*i);
            }
            /* the joint transitions are the product of the two respective transitions */
            tr_ = other->tr_ * rabin->transitions_;
            live_ = other->live_ * rabin->transitions_;
            /* compute the helper BDDs for synthesis */
            cubePost_ = base_.cube(helper::toUBDD<UBDD>(base_, helper::sort(postVars_)));
            if (other_variables.empty())
                cubeOther_ = base_.one();
            else
                cubeOther_ = base_.cube(helper::toUBDD<UBDD>(base_, helper::sort(other_variables)));
            /* the rabin pairs are directly inherited from the rabin automaton */
            RabinPairs_ = rabin->RabinPairs_;

            adjust_for_approximation_type();

        }

        void adjust_for_approximation_type() {
            if (!strcmp(str_, "over")) {
                UBDD live_domain = live_.existAbstract(CubeNotState());
                UBDD nonlive_env_nodes = (tr_ & (!live_)).existAbstract(CubeNotState());
                if ((live_domain & nonlive_env_nodes) != base_.zero()) {// todo check
                    std::cerr << "An over-approximation is only implemented for stochastic games (where nonlive and live vertices are disjoint)";
                } else {
                    sys_nodes_ = nodes_ & (!live_domain);
                    env_nodes_ = live_domain;
                }
            } else if (!strcmp(str_, "under")) {
            }// do nothing
            else if (!strcmp(str_, "wc")) {
                live_ = base_.zero();
            } else {
                std::cerr << "Invalid argument for str." << '\n';
            }
        }

        inline UBDD CubeNotState() {
            return cubePost_ * cubeOther_;
        }

        UBDD cpre(const UBDD &Zi) {
            UBDD Z = Zi;
            /* project onto state alphabet */
            Z = Z.existAbstract(CubeNotState());
            /* swap variables */
            Z = Z.permute(preVars_, postVars_);
            /* the controllable system edges */
            UBDD W0 = tr_ & Z & sys_nodes_;
            /* the controllable environment edges */
            UBDD nZ = !Z & nodes_;
            /* the environment nodes having an outgoing edge outside Zi */
            UBDD F = tr_.andAbstract(nZ, CubeNotState()) & env_nodes_;
            /* the other environment nodes are controllable */
            UBDD nF = !F;
            UBDD W1 = tr_ & nF & env_nodes_;
            /* return all the controllable edges */
            return (W0 | W1);
        }

        UBDD apre(const UBDD &Y,
                  const UBDD &Z) {
            UBDD Z2 = Z;
            /* project onto state alphabet */
            Z2 = Z2.existAbstract(CubeNotState());
            /* swap variables */
            Z2 = Z2.permute(preVars_, postVars_);
            /* the nodes from which there are live edges to Z */
            UBDD W0 = live_.andAbstract(Z2, CubeNotState());
            /* remove from W0 those env vertices which have some transition outside Y */
            UBDD P = cpre(Y);
            UBDD W1 = W0 & P;
            /* the edges in cpre(Z) are also in apre(Y,Z) */
            UBDD W2 = W1 | cpre(Z);
            return W2;
        }

        UBDD RabinRecurse(UBDD controller,
                          genie::const_arg_recursive_rabin<UBDD> rrConst,
                          genie::nconst_arg_recursive_rabin<UBDD> rrVars);

        void print_bdd_info(const UBDD &store,
                            const std::vector<size_t> &preVars_,
                            const int verbose = 2) {
            if (verbose == 2) {
                size_t nvars = preVars_.size();
                for (int i = 0; i < pow(2, preVars_.size()); i++) {
                    UBDD cur_node = helper::elementToBdd<UBDD>(base_, i, preVars_, nvars);
                    UBDD intermediate = cur_node & store;
                    if (intermediate != store.zero()) {
                        cout << i << " ";
                    }
                }
                cout << -1 << " ";
            }
            if (verbose == 1) {
                cout << store.existAbstract(CubeNotState()).countMinterm(preVars_.size()) << "\n";
            }
        }

        void print_rabin_info(const UBDD &store,
                              const char *mode,
                              int verbose,
                              int iteration = 0,
                              int depth = 0) {
            if (verbose >= 1 && (!strcmp(mode, "X") || !strcmp(mode, "Y"))) {
                std::cout << std::endl;
                bool X_mode = !strcmp(mode, "X");
                if (X_mode) {
                    printTabs(3 * depth + 1);
                    std::cout << "X" << depth << ", iteration ";
                } else {
                    printTabs(3 * depth);
                    std::cout << "Y" << depth << ", iteration ";
                }
                std::cout << iteration << ", states = ";
                print_bdd_info(store, preVars_, verbose);
            }
            if (verbose == 2 && !strcmp(mode, "end")) {
                size_t nvars = preVars_.size();

                std::cout << std::endl;
                std::cout << "\t Asked to print states = ";
                for (int i = 0; i < pow(2, preVars_.size()); i++) {
                    std::cout << i << " ";
                    UBDD cur_node = helper::elementToBdd<UBDD>(base_, i, preVars_, nvars);
                    for (int j = 0; j < pow(2, preVars_.size()); j++) {
                        UBDD temp = store.zero();
                        UBDD succ_node = helper::elementToBdd(base_, j, postVars_, nvars);
                        temp = (cur_node & succ_node);
                        UBDD intermediate = temp & store;

                        if (intermediate != store.zero()) {
                            std::cout << " " << j << " ";
                        }
                    }
                    std::cout << "-1\n\n";
                }
            }
        }

        UBDD RabinEnd(UBDD C) {
            UBDD strategy = (C & sys_nodes_);
            return strategy;
        }


    }; /* close class def */

}// namespace fairsyn

#endif /* FIXPOINT_HH_ */
