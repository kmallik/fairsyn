/** @file RabinAutomaton.hh
 */

#ifndef RABINAUTOMATON_HH_
#define RABINAUTOMATON_HH_

#include <cmath>
#include <cstdlib>
#include <fstream>

#include "Helper.hh"
#include "lib/BaseRabinAutomaton.hh"
#include "utils/hoa_consumer_build_rabin.hh"

using namespace genie;

namespace fairsyn {

    /** Symbolic (bdd) implementation of a Rabin automaton */
    template <class UBDD>
    class RabinAutomaton {
    public:
        UBDD base_; /**< the bdd manager*/
        UBDD stateSpace_;
        std::vector<size_t> stateVars_;       /**< stores the "pre" state variable indices */
        std::vector<size_t> postStateVars_;   /**< stores the "post" state variable indices */
        UBDD transitions_;                    /**< @brief the bdd representing the transition relation
                                               * @details the bdd representing the transition relation as sets of tuples (s,x',s'),
                                               * where s is pre-state of the rabin automaton, x' is post-state of the symbolic model,
                                               * and s' is the post-state of the rabin automaton */
        size_t numRabinPairs_;                /**< the number of rabin pairs */
        std::vector<rabin_pair_<UBDD>> RabinPairs_; /**< @brief vector of sets of states of the automaton
                                               * @details BDD vector[numRabinPairs_][2] containing the rabin pairs {(G_i,~R_i)},
                                               * where the Rabin condition is given as:
                                               * \/_i ([]<>G_i & <>[]~R_i)
                                               * and each G_i,~R_i are given a bdd representing a set of states of the automaton */

        //        friend class Fixpoint; todo Do we need this?

    public:
        /**
         * constructs the bdd representation of the Rabin automaton from a given list representation of the states and the transitions
        */
        RabinAutomaton(UBDD &base,
                       std::set<size_t> &all_variables,
                       std::map<std::string, UBDD> input_pred_to_bdd,
                       const std::string &filename) {
            base_ = base;
            std::ifstream file(filename);
            cpphoafparser::HOAConsumer::ptr consumer;
            cpphoafparser::rabin_data data;
            consumer.reset(new cpphoafparser::HOAConsumerBuildRabin(&data));
            cpphoafparser::HOAParser::parse(file, consumer);
            std::map<std::string, UBDD> input_id_to_bdd;
            for (auto it = data.ap_id_map.begin(); it != data.ap_id_map.end(); ++it) {
                size_t key = it->first;
                input_id_to_bdd.insert({std::to_string(key), input_pred_to_bdd[data.ap_id_map[key]]});
            }
            /* create a BDD representation of the state space of the rabin automaton */
            std::vector<size_t> state_ids;
            for (size_t i = 0; i < data.States; i++) {
                state_ids.push_back(i);
            }
            size_t nvars = (size_t)std::ceil(std::log2(data.States));
            size_t max_var_id = *(all_variables.rbegin());
            /* the "pre" bdd variables */
            for (size_t i = 1; i <= nvars; i++) {
                size_t new_var_id = max_var_id + i;
                stateVars_.push_back(new_var_id);
                all_variables.insert(new_var_id);
            }
            /* the "post" bdd variables */
            for (size_t i = 1; i <= nvars; i++) {
                size_t new_var_id = max_var_id + stateVars_.size() + i;
                postStateVars_.push_back(new_var_id);
                all_variables.insert(new_var_id);
            }
            /* the bdd representing the set of states
             * given in terms of the "pre" variables */
            stateSpace_ = helper::setToBdd<UBDD>(base_, state_ids, stateVars_, nvars);
            /* build the transitions bdd */
            transitions_ = base_.zero();
            for (size_t i = 0; i < data.Transitions.size(); i++) {
                UBDD cube = base_.one();
                size_t v = data.Transitions[i].state_id;
                cube &= helper::elementToBdd<UBDD>(base_, v, stateVars_, nvars);
                std::stack<cpphoafparser::HOAConsumer::label_expr::ptr> nodes;
                nodes.push(data.Transitions[i].label);
                while (nodes.empty()) {
                    cpphoafparser::HOAConsumer::label_expr::ptr curr_node = nodes.top();
                    nodes.pop();
                    if (curr_node->isAND()) {
                        nodes.push(curr_node->getLeft());
                        nodes.push(curr_node->getRight());
                    } else if (curr_node->isNOT()) {
                        cube &= !(input_id_to_bdd[curr_node->getLeft()->toString()]);
                    } else if (curr_node->isAtom()) {
                        cube &= input_id_to_bdd[curr_node->toString()];
                    }
                }
                v = data.Transitions[i].post_id;
                cube &= helper::elementToBdd<UBDD>(base_, v, postStateVars_, nvars);
                transitions_ |= cube;
            }
            /* build the rabin pairs in terms of the pre variables */
            numRabinPairs_ = data.acc_pairs.size();
            for (size_t i = 0; i < numRabinPairs_; i++) {
                rabin_pair_<UBDD> pair;
                pair.rabin_index_ = i;
                /* first, create the BDD for the G sets */
                std::vector<size_t> v = {data.acc_signature[data.acc_pairs[i][0]]};
                pair.G_ = base_.zero();
                for (size_t j = 0; j < v.size(); j++) {
                    size_t temp = v[j];
                    pair.G_ |= helper::elementToBdd<UBDD>(base_, temp, stateVars_, nvars);
                }
                /* second, create the BDD for the COMPLEMENT OF the R sets */
                v = {data.acc_signature[data.acc_pairs[i][1]]};
                UBDD R = base_.zero();
                for (size_t j = 0; j < v.size(); j++) {
                    size_t temp = v[j];
                    R |= helper::elementToBdd<UBDD>(base_, temp, stateVars_, nvars);
                }
                pair.nR_ = (!R) & stateSpace_;
                //            for (size_t j=static_cast<size_t>(stateSpace_->getFirstGridPoint()[0]); j<=static_cast<size_t>(stateSpace_->getLastGridPoint()[0]); j++) {
                //                bool isInR=false;
                //                for (size_t k=0; k<v.size(); k++) {
                //                    if (v[k] == j) {
                //                        isInR=true;
                //                        break;
                //                    }
                //                }
                //                if (!isInR) {
                //                    std::vector<size_t> temp={j};
                //                    pair.nR_ |= stateSpace_->elementToMinterm(temp);
                //                }
                //            }
                RabinPairs_.push_back(pair);
            }
        }
    }; /* close class def */
}// namespace fairsyn

#endif /* RABINAUTOMATON_HH_ */
