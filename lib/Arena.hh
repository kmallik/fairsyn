/** @file Arena.hh
 */

#ifndef ARENA_HH_
#define ARENA_HH_

#include "Helper.hh"
#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <stdexcept>
#include <vector>



namespace fairsyn {

    template <class UBDD>
    struct thread {
        std::vector<size_t> state_vars;
        std::vector<size_t> state_vars_post;
        // std::vector<size_t> action_vars;
        UBDD states;
        UBDD transitions;
    };

    /**
     * @brief a representation of the finite two-player turn-based game arena with edge fairness condition
     */
    template <class UBDD>
    class Arena {
    public:
        UBDD base_;                    /**< the bdd manager */
        UBDD nodes_;                   /**< stores the nodes in a BDD */
        UBDD sys_nodes_;               /**< stores the system nodes in a BDD */
        UBDD env_nodes_;               /**< stores the environment nodes in a BDD */
        std::vector<size_t> preVars_;  /**< stores the "pre" node variable indices */
        std::vector<size_t> postVars_; /**< stores the "post" node variable indices  */
        UBDD tr_;                      /**< Transition BDD */
        UBDD live_;                    /**< the live transitions (subset of the transition relation) */


    public:
        /**
         *  @brief construct Arena from a given product of thread interfaces
         */
        Arena(UBDD &base,
              std::set<size_t> &all_variables,
              std::vector<thread<UBDD> *> &thread_list,
              std::vector<size_t> &resource_vars,
              UBDD resource_states,
              std::vector<size_t> &resource_vars_post,
              std::vector<size_t> &input_action_vars,
              std::vector<size_t> &output_action_vars,
              UBDD input_action_states,
              UBDD output_action_states,
              std::map<std::string, UBDD> &progress_pred_to_bdd) {
            base_ = base;
            for (size_t i = 0; i < thread_list.size(); i++) {
                for (auto j = thread_list[i]->state_vars.begin(); j != thread_list[i]->state_vars.end(); ++j) {
                    all_variables.insert(*j);
                    preVars_.push_back(*j);
                }
                for (auto j = thread_list[i]->state_vars_post.begin();
                     j != thread_list[i]->state_vars_post.end(); ++j) {
                    all_variables.insert(*j);
                    postVars_.push_back(*j);
                }
            }
            for (auto j = resource_vars.begin(); j != resource_vars.end(); ++j) {
                all_variables.insert(*j);
                preVars_.push_back(*j);
            }
            for (auto j = resource_vars_post.begin(); j != resource_vars_post.end(); ++j) {
                all_variables.insert(*j);
                postVars_.push_back(*j);
            }
            /* NOTE: what to do with the action_vars: they go to preVars_ or postVars_? Or do they remain separate? I think they should remain separate (implicitly appears on the transitions). */
            for (auto j = input_action_vars.begin(); j != input_action_vars.end(); ++j) {
                all_variables.insert(*j);
            }
            for (auto j = output_action_vars.begin(); j != output_action_vars.end(); ++j) {
                all_variables.insert(*j);
            }
            /* ----------- the state space -----------------
             * state space of the product is the product of the state spaces of the threads and the resource state space */
            nodes_ = base_.one();
            for (size_t i = 0; i < thread_list.size(); i++) {
                nodes_ &= thread_list[i]->states;
            }

            nodes_ &= resource_states;

            /* ------------ the transitions -----------------*/
            std::vector<size_t> thread_vars, thread_vars_post; /* bdd variables for representing which of the threads moves in a transition
        When all the variables are 0, it means that no thread moves (for example, from the system nodes). */
            for (size_t i = 1; i <= std::ceil(std::log2(thread_list.size() + 1)); i++) {
                size_t new_var_id = *all_variables.rbegin() + 1;
                thread_vars.push_back(new_var_id);
                preVars_.push_back(new_var_id);
                all_variables.insert(new_var_id);
            }
            for (size_t i = 1; i <= std::ceil(std::log2(thread_list.size() + 1)); i++) {
                size_t new_var_id = *all_variables.rbegin() + 1;
                thread_vars_post.push_back(new_var_id);
                postVars_.push_back(new_var_id);
                all_variables.insert(new_var_id);
            }
            /* build the product transition with the following constraints:
             * - at a time, only one thread moves to the next step and the rest remains standstill
             * - the index of the moving thread is recorded in the thread_vars_post bdd variables
             */
            tr_ = base_.zero();
            for (size_t i = 0; i < thread_list.size(); i++) {
                /* the bdd representing the transitions where only thread i moves */
                UBDD T = base_.one();
                /* thread i moves */

                T &= helper::elementToBdd<UBDD>(base_, i + 1, thread_vars_post, thread_vars_post.size()); /* select thread i; the "+1" is to account for the fact that the decimal value "0" is reserved for the case when no thread moves. */
                T &= thread_list[i]->transitions;
                /* rest of the threads remain at standstill */
                for (size_t j = 0; j < thread_list.size(); j++) {

                    if (i == j) {
                        continue;
                    }

                    for (size_t k = 0; k < thread_list[j]->state_vars.size(); k++) {
                        /* b represents a bdd of the form (x <=> x'), where x and x' are the k-th pre and post state variables of the thread j */
                        UBDD b = base_.var(thread_list[j]->state_vars[k]) &
                                 base_.var(thread_list[j]->state_vars_post[k]);
                        b |= (!base_.var(thread_list[j]->state_vars[k])) &
                             (!base_.var(thread_list[j]->state_vars_post[k]));
                        T &= b;
                    }
                }

                tr_ |= T;
            }
            tr_ &= nodes_;
            /* return the mapping from progress_i symbols to the state predicates recording the progress in the individual threads */
            progress_pred_to_bdd.clear();
            for (size_t i = 0; i < thread_list.size(); i++) {
                std::string s = "progress_";
                s += std::to_string(i);
                // progress_pred_to_bdd.insert({s, nodes_ & elementToBdd(i+1, thread_vars_bdd, thread_vars.size())}); /* mark progress in thread i; the "+1" is to account for the fact that the decimal value "0" is reserved for the case when no thread moves. */
                /* mark progress in thread i; the "+1" is to account for the fact that the decimal value "0" is reserved for the case when no thread moves. */
                progress_pred_to_bdd.insert({s, nodes_.permute(preVars_, postVars_) &
                                                        helper::elementToBdd<UBDD>(base_, i + 1, thread_vars_post,
                                                                                   thread_vars_post.size())});
            }
            /* ---------------- the game (splitting states into system and environment states) ------------ */
            size_t system_nodes = *all_variables.rbegin() +
                                    1; /* bdd variable that is 1 for system nodes and 0 for the environment nodes */
            UBDD system_nodes_bdd = base_.var(system_nodes);
            UBDD environment_nodes_bdd = !system_nodes_bdd;
            sys_nodes_ = nodes_ & system_nodes_bdd;
            env_nodes_ = nodes_ & (!system_nodes_bdd);
            preVars_.push_back(system_nodes);
            all_variables.insert(system_nodes);
            size_t system_nodes_post = *all_variables.rbegin() + 1; /* the post bdd variable */
            UBDD system_nodes_post_bdd = base_.var(system_nodes_post);
            UBDD environment_nodes_post_bdd = !system_nodes_post_bdd;
            postVars_.push_back(system_nodes_post);
            all_variables.insert(system_nodes_post);
            /* encode the interplay between the system and the environment */
            UBDD game_rule = base_.zero();
            /* from the system nodes, the game can move to one of these:
             * (1) an input successor
             * (2) the environment node which has the same valuations for the rest of the bdd variables */
            /* rule (1) */
            game_rule |= (system_nodes_bdd & input_action_states & (!output_action_states) & system_nodes_post_bdd);

            /* rule (2) */
            UBDD b = base_.one();
            b = system_nodes_bdd & environment_nodes_post_bdd;
            for (size_t k = 0; k < preVars_.size(); k++) {
                if (preVars_[k] == system_nodes) {
                    continue;
                }
                /* b represents a bdd of the form (x <=> x'), where x and x' are the k-th pre and post state variables of the product state space */
                UBDD c = base_.var(preVars_[k]) & base_.var(postVars_[k]);
                c |= (!base_.var(preVars_[k])) & (!base_.var(postVars_[k]));
                b &= c;
            }
            game_rule |= b;

            /* from the envrionment nodes, the game can move along one of the output successors, and the next state is a sysetm node */
            game_rule |= (environment_nodes_bdd & output_action_states & (!input_action_states) &
                          system_nodes_post_bdd);

            /* any system node can transition to an environment node without changing the other components */
            b = base_.one();
            b &= base_.var(system_nodes) & (!base_.var(system_nodes_post));
            for (size_t k = 0; k < preVars_.size(); k++) {
                if (preVars_[k] == system_nodes) {
                    continue;
                }
                /* b represents a bdd of the form (x XOR x'), where x and x' are the k-th pre and post state variables of the product state space */
                UBDD c = base_.var(preVars_[k]) & base_.var(postVars_[k]);
                c |= (!base_.var(preVars_[k])) & (!base_.var(postVars_[k]));
                b &= c;
            }
            tr_ |= b;
            tr_ &= nodes_;

            /* finally, incorporate the system-environment interaction in the transitions */
            tr_ &= game_rule;

            /* the live transitions are all the environment transitions */
            live_ = tr_ & environment_nodes_bdd;
            /* sort the list of variables */
            std::sort(preVars_.begin(), preVars_.end());
            std::sort(postVars_.begin(), postVars_.end());
        }

        /**
         * @brief constructor Arena
         */
        Arena(UBDD &base,
              std::set<size_t> &all_variables,
              std::vector<size_t> &nodes,
              std::vector<size_t> &sys_nodes,
              std::vector<size_t> &env_nodes,
              std::vector<std::vector<size_t>> &transitions,
              std::vector<std::vector<size_t>> &live_edges) {
            base_ = base;
            /* number of bdd variables required for the nodes */
            size_t nvars = std::ceil(std::log2(nodes.size()));
            if (all_variables.empty()) {
                /* the "pre" bdd variables */
                for (size_t i = 0; i < nvars; i++) {
                    size_t new_var_id = i;
                    preVars_.push_back(new_var_id);
                    all_variables.insert(new_var_id);
                }
            } else {
                /* the "pre" bdd variables */
                size_t max_var_id = *(all_variables.rbegin());
                for (size_t i = 1; i <= nvars; i++) {
                    size_t new_var_id = max_var_id + i;
                    preVars_.push_back(new_var_id);
                    all_variables.insert(new_var_id);
                }
            }
            size_t max_var_id = *(all_variables.rbegin());

            /* the "post" bdd variables */
            for (size_t i = 1; i <= nvars; i++) {
                size_t new_var_id = max_var_id + i;
                postVars_.push_back(new_var_id);
                all_variables.insert(new_var_id);
            }
            /* the bdd representing the set of nodes
             * given in terms of the "pre" variables */

            nodes_ = helper::setToBdd<UBDD>(base_, nodes, preVars_, nvars);
            sys_nodes_ = helper::setToBdd<UBDD>(base_, sys_nodes, preVars_, nvars);
            env_nodes_ = helper::setToBdd<UBDD>(base_, env_nodes, preVars_, nvars);
            /* the bdd representing the transition relation */
            tr_ = base_.zero();
            for (size_t i = 0; i < transitions.size(); i++) {
                /* the i-th element of the transitions vector is the vector of successors from the i-th vertex */
                UBDD cur_node = helper::elementToBdd<UBDD>(base_, i, preVars_, nvars);
                UBDD succ_node = helper::setToBdd<UBDD>(base_, transitions[i], postVars_, nvars);
                tr_ += (cur_node & succ_node);
            }

            /*for asserting whether BDDs formed are right or not */
            for (size_t i = 0; i < transitions.size(); i++) {
                UBDD cur_node = helper::elementToBdd<UBDD>(base_, i, preVars_, nvars);

                for (size_t j = 0; j < transitions[i].size(); j++) {
                    UBDD temp = base_.zero();
                    UBDD succ_node = helper::elementToBdd<UBDD>(base_, transitions[i][j], postVars_, nvars);
                    temp = (cur_node & succ_node);
                    UBDD intermediate = temp & tr_;
                    if (temp != intermediate) {
                        cout << " BDDs not formed well \n";
                        exit(0);
                    }
                }
            }


            /* the bdd representing the live edges */
            live_ = base_.zero();
            for (size_t i = 0; i < live_edges.size(); i++) {
                /* the i-th element of the live transitions vector is the vector of live edge successors from the i-th vertex */
                UBDD cur_node = helper::elementToBdd<UBDD>(base_, i, preVars_, nvars);
                UBDD succ_node = helper::setToBdd<UBDD>(base_, live_edges[i], postVars_, nvars);
                live_ += (cur_node & succ_node);
            }

            /* for asserting that the system nodes and the environment nodes form a partition only */
            if (((sys_nodes_ & env_nodes_) != base_.zero()) |
                ((sys_nodes_ | env_nodes_) != nodes_)) {
                cout << "The system and the environment nodes do not form a partition. Exiting.\n";
                exit(0);
            }

            /* for asserting that the live edges are from env vertices only */
            if ((live_ & sys_nodes_) != base_.zero()) {
                cout << "Some live edges start from the system nodes. Exiting.\n";
                exit(0);
            }

            /* for asserting that transitions are valid */
            if (!(tr_ <= (nodes_ & nodes_.permute(preVars_, postVars_)))) {
                cout << "Some transitions are not valid. Exiting.\n";
                exit(0);
            }

            /* for asserting that the live edges are subsets of the transitions */
            if (!(live_ <= tr_)) {
                cout << "Some live edges are not valid transitions. Exiting.\n";
                exit(0);
            }
        }

        /**
         * @brief Default constructor
         */
        Arena(UBDD &base) {
            base_ = base;
            nodes_ = base_.zero();
            sys_nodes_ = base_.zero();
            env_nodes_ = base_.zero();
            tr_ = base_.zero();
            live_ = base_.zero();
        }

        /**
         * @brief This function is only for debugging
         */
        void print_number_of_states(UBDD ubdd, std::string ubdd_name) {
            UBDD cubePost_ = base_.cube(helper::toUBDD<UBDD>(base_, helper::sort(postVars_)));
            UBDD cubeOther_ = base_.cube(helper::toUBDD<UBDD>(base_, {4,5}));
            cout << ubdd_name << ": " <<  ubdd.existAbstract(cubePost_ * cubeOther_).countMinterm(preVars_.size()) << "\n";
        }

    }; /* close class definition */
}// namespace fairsyn

#endif /* ARENA_HH_ */
