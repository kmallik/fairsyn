/* The Lego example from the code-aware resource management paper */
#include "Arena.hh"
#include "Fixpoint.hh"
#include "RabinAutomaton.hh"
#include "lib/BaseFixpoint.hh"
#include "lib/ParallelRabinRecurse.hh"
#include "testlib.h"
#include "ubdd/CuddUBDD.hh"
#include "ubdd/SylvanUBDD.hh"
#include "utils/TicToc.hh"

#include <algorithm>// for random shuffle
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <set>
#include <sstream>
#include <utility>// for the class called pair

using namespace fairsyn;

#define accl_on 1
#define verbose 0
#define n_workers 0

std::vector<size_t> resource_vars;
std::vector<size_t> non_resource_vars;
std::vector<size_t> resource_vars_post;
std::vector<size_t> input_action_vars;
std::vector<size_t> output_action_vars;


int str2int(string temp) {
    string s = temp;
    if (s[0] == '-') {
        return str2int(s.substr(1)) * -1;
    }
    std::stringstream ss(s);
    int x = 0;
    ss >> x;
    return x;
}

bool find_in(int n, std::vector<size_t> v) {
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == n)
            return true;
    }
    return false;
}

template <class UBDD>
void add_connection(const UBDD &base,
                    thread<UBDD> &thr,
                    size_t x_state,
                    size_t x_state_post,
                    size_t x_output,
                    size_t x_input) {
    UBDD rule = base.one();
    rule &= helper::elementToBdd(base, x_state, thr.state_vars, thr.state_vars.size());
    rule &= helper::elementToBdd(base, x_state_post, thr.state_vars_post, thr.state_vars_post.size());
    rule &= helper::elementToBdd(base, x_output, output_action_vars, output_action_vars.size());
    rule &= helper::elementToBdd(base, x_input, input_action_vars, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        UBDD b = base.var(resource_vars[i]) & base.var(resource_vars_post[i]);
        b |= (!base.var(resource_vars[i])) & (!base.var(resource_vars_post[i]));
        rule &= b;
    }

    thr.transitions |= rule;
}

template <class UBDD>
void add_connection(const UBDD &base,
                    thread<UBDD> &thr,
                    size_t x_state,
                    size_t x_state_post,
                    size_t x_output,
                    size_t x_input,
                    size_t semi_rule_size,
                    const std::vector<size_t> &semi_rule_vars,
                    const std::vector<size_t> &semi_rule_vars_post,
                    bool reverse_order = false) {
    UBDD rule = base.one();
    rule &= helper::elementToBdd(base, x_state, thr.state_vars, thr.state_vars.size());
    rule &= helper::elementToBdd(base, x_state_post, thr.state_vars_post, thr.state_vars_post.size());
    rule &= helper::elementToBdd(base, x_output, output_action_vars, output_action_vars.size());
    rule &= helper::elementToBdd(base, x_input, input_action_vars, input_action_vars.size());

    UBDD semi_rule = base.zero();
    if (reverse_order) {
        for (int i = 0; i < semi_rule_size; i++) {
            semi_rule |= (helper::elementToBdd(base, i, semi_rule_vars, semi_rule_vars.size()) &
                          helper::elementToBdd(base, i + 1, semi_rule_vars_post, semi_rule_vars_post.size()));
        }
    } else {
        for (int i = 1; i <= semi_rule_size; i++) {
            semi_rule |= (helper::elementToBdd(base, i, semi_rule_vars, semi_rule_vars.size()) &
                          helper::elementToBdd(base, i - 1, semi_rule_vars_post, semi_rule_vars_post.size()));
        }
    }
    rule &= semi_rule;

    for (int i = 0; i < resource_vars.size(); i++) {
        if (find_in(i, semi_rule_vars))
            continue;
        UBDD b = base.var(resource_vars[i]) & base.var(resource_vars_post[i]);
        b |= (!base.var(resource_vars[i])) & (!base.var(resource_vars_post[i]));
        rule &= b;
    }

    thr.transitions |= rule;
}

SylvanUBDD run_rabin(Fixpoint<SylvanUBDD> fp, size_t M, bool parallel) {
    SylvanUBDD result;
    if (parallel) {
        cout << "START PARALLEL RABIN\n";
        result = fp.RabinEnd(fp.Rabin(accl_on, M, fp.nodes_, verbose, genie::ParallelRabinRecurse));
        cout << "DONE PARALLEL RABIN\n";
    } else {
        cout << "START SEQUENTIAL RABIN\n";
        result = fp.RabinEnd(fp.Rabin(accl_on, M, fp.nodes_, verbose));
        cout << "DONE SEQUENTIAL RABIN\n";
    }
    return result;
}


CuddUBDD run_rabin(Fixpoint<CuddUBDD> fp, size_t M, bool parallel) {
    cout << "START SEQUENTIAL RABIN\n";
    CuddUBDD result = fp.RabinEnd(fp.Rabin(accl_on, M, fp.nodes_, verbose));
    cout << "DONE SEQUENTIAL RABIN\n";
    return result;
}

template <class UBDD>
void run_experiment(size_t br_size, size_t out_size,
                    const std::string &log_filename,
                    int M, bool parallel, size_t is_sylvan) {
    UBDD base;
    std::fstream logfile;

    logfile.open(log_filename, std::fstream::app);

    size_t cur_vars = 0;

    size_t br_bits = std::ceil(std::log2(br_size + 1));
    size_t out_bits = std::ceil(std::log2(out_size + 1));

    logfile << "\n Broadcast size: " << br_size << "  Output size:  " << out_size << "  Memory size: " << M;
    if (parallel)
        logfile << "  UBDD: sylvan  ";
    else
        logfile << "  UBDD: cudd  ";

    logfile.close();

    ///////////////////////////

    for (int i = 0; i < (2 + 2 * br_bits + 2 * out_bits); i++) {
        resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    for (int i = 0; i < (2 + 2 * br_bits + 2 * out_bits); i++) {
        resource_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    //output actions -- 1

    for (int i = 0; i < 1; i++) {
        output_action_vars.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }

    //input actions -- 1

    for (int i = 0; i < 1; i++) {
        input_action_vars.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }

    ///////////////////////
    UBDD resource_states;
    if (is_sylvan) {
        std::vector<uint8_t> resource_state_var_values;
        uint8_t dont_care = 2;
        for (size_t i = 0; i < (2 + 2 * br_bits + 2 * out_bits); i++) {
            resource_state_var_values.push_back(dont_care);
        }
        resource_states = base.cube(helper::toUBDD(base, resource_vars), resource_state_var_values);
    } else {
        resource_states = base.one();
    }

    UBDD input_action_states = base.zero();
    for (int i = 1; i < 2; i++) {
        input_action_states |= helper::elementToBdd<UBDD>(base, i, input_action_vars, input_action_vars.size());
    }
    UBDD output_action_states = base.zero();
    for (int i = 1; i < 2; i++) {
        output_action_states |= helper::elementToBdd<UBDD>(base, i, output_action_vars, output_action_vars.size());
    }

    ///////  broadcast variables

    std::vector<size_t> broadcast_empty, broadcast_empty_post;
    for (int i = 0; i < br_bits; i++) {
        broadcast_empty.push_back(i);
        broadcast_empty_post.push_back(i + (2 + 2 * br_bits + 2 * out_bits));
    }

    std::vector<size_t> broadcast_full, broadcast_full_post;
    for (int i = 0; i < br_bits; i++) {
        broadcast_full.push_back(i + br_bits);
        broadcast_full_post.push_back(i + br_bits + (2 + 2 * br_bits + 2 * out_bits));
    }

    std::vector<size_t> broadcast_lock, broadcast_lock_post;
    for (int i = 0; i < 1; i++) {
        broadcast_lock.push_back(i + 2 * br_bits);
        broadcast_lock_post.push_back(i + 2 * br_bits + (2 + 2 * br_bits + 2 * out_bits));
    }


    ///////  output variables
    std::vector<size_t> output_empty, output_empty_post;
    for (int i = 0; i < out_bits; i++) {
        output_empty.push_back(i + 2 * br_bits + 1);
        output_empty_post.push_back(i + 2 * br_bits + 1 + (2 + 2 * br_bits + 2 * out_bits));
    }

    std::vector<size_t> output_full, output_full_post;
    for (int i = 0; i < out_bits; i++) {
        output_full.push_back(i + 2 * br_bits + 1 + out_bits);
        output_full_post.push_back(i + 2 * br_bits + 1 + out_bits + (2 + 2 * br_bits + 2 * out_bits));
    }

    std::vector<size_t> output_lock, output_lock_post;
    for (int i = 0; i < 1; i++) {
        output_lock.push_back(i + 2 * br_bits + 1 + 2 * out_bits);
        output_lock_post.push_back(i + 2 * br_bits + 1 + 2 * out_bits + (2 + 2 * br_bits + 2 * out_bits));
    }


    // declaring the states
    thread<UBDD> router;
    for (int i = 0; i < 4; i++) {
        router.state_vars.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    for (int i = 0; i < 4; i++) {
        router.state_vars_post.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    router.states = base.zero();
    for (int i = 0; i < 11; i++) {
        router.states |= helper::elementToBdd<UBDD>(base, i, router.state_vars, router.state_vars.size());
    }
    router.transitions = base.zero();

    // generate the rules

    //  0 --> 1
    add_connection(base, router, 0, 1, 1, 0);

    //1 --> 2
    //broadcast_empty_1
    add_connection(base, router, 1, 2, 0, 1, br_size, broadcast_empty, broadcast_empty_post);

    // 2 --> 3
    add_connection(base, router, 2, 3, 1, 0);

    // 3 --> 4
    //broadcast_lock_1
    add_connection(base, router, 3, 4, 0, 1, 1, broadcast_lock, broadcast_lock_post);

    //  4 --> 5
    //broadcast_lock_2
    add_connection(base, router, 4, 5, 1, 0, 1, broadcast_lock, broadcast_lock_post, true);


    //    5 --> 0
    //broadcast_full_2
    add_connection(base, router, 5, 0, 1, 0, br_size, broadcast_full, broadcast_full_post, true);

    // generate the rules
    //  0 --> 6
    add_connection(base, router, 0, 6, 1, 0);

    //6 --> 7
    //output_empty_1
    add_connection(base, router, 6, 7, 0, 1, out_size, output_empty, output_empty_post);

    // 7 --> 8
    add_connection(base, router, 7, 8, 1, 0);

    // 8 --> 9
    //output_lock_1
    add_connection(base, router, 8, 9, 0, 1, 1, output_lock, output_lock_post);

    //  9 --> 10
    //output_lock_2
    add_connection(base, router, 9, 10, 1, 0, 1, output_lock, output_lock_post, true);

    //  10 --> 0
    //output_full_2
    add_connection(base, router, 10, 0, 1, 0, out_size, output_full, output_full_post, true);

    cout << "DONE Router\n";


    //////////////////////////////////////////////////////////////////

    // delay
    // declaring the states
    thread<UBDD> delay;
    for (int i = 0; i < 4; i++) {
        delay.state_vars.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    for (int i = 0; i < 4; i++) {
        delay.state_vars_post.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    delay.states = base.zero();
    for (int i = 0; i < 12; i++) {
        delay.states |= helper::elementToBdd<UBDD>(base, i, delay.state_vars, delay.state_vars.size());
    }
    delay.transitions = base.zero();

    // generate the rules

    //  0 --> 1
    add_connection(base, delay, 0, 1, 1, 0);

    //1 --> 2
    //br_full_1
    add_connection(base, delay, 1, 2, 0, 1, br_size, broadcast_full, broadcast_full_post);

    // 2 --> 3
    add_connection(base, delay, 2, 3, 1, 0);

    // 3 --> 4
    //out_empty_1
    add_connection(base, delay, 3, 4, 0, 1, out_size, output_empty, output_empty_post);

    //  4 --> 5
    add_connection(base, delay, 4, 5, 1, 0);

    //    5 --> 6
    //br_lock_1
    add_connection(base, delay, 5, 6, 0, 1, 1, broadcast_lock, broadcast_lock_post);

    //    6 --> 7
    add_connection(base, delay, 6, 7, 1, 0);

    //    7 --> 8
    //out_lock_1
    add_connection(base, delay, 7, 8, 0, 1, 1, output_lock, output_lock_post);

    //    8 --> 9
    //br_lock_2
    add_connection(base, delay, 8, 9, 1, 0, 1, broadcast_lock, broadcast_lock_post, true);

    //    9 --> 10
    //out_lock_2
    add_connection(base, delay, 9, 10, 1, 0, 1, output_lock, output_lock_post, true);

    //    10 --> 11
    //out_full_2
    add_connection(base, delay, 10, 11, 1, 0, out_size, output_full, output_full_post, true);

    //    11 --> 0
    //br_empty_2
    add_connection(base, delay, 11, 0, 1, 0, br_size, broadcast_empty, broadcast_empty_post, true);

    cout << "DONE delay\n";

    ////////////////////////////////

    // sender
    // declaring the states
    thread<UBDD> sender;
    for (int i = 0; i < 5; i++) {
        sender.state_vars.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    for (int i = 0; i < 5; i++) {
        sender.state_vars_post.push_back(cur_vars);
        non_resource_vars.push_back(cur_vars);
        ++cur_vars;
    }
    sender.states = base.zero();
    for (int i = 0; i < 17; i++) {
        sender.states |= helper::elementToBdd<UBDD>(base, i, sender.state_vars, sender.state_vars.size());
    }
    sender.transitions = base.zero();

    // generate the rules

    //  0 --> 1
    add_connection(base, sender, 0, 1, 1, 0);

    //1 --> 2
    //out_full_1
    add_connection(base, sender, 1, 2, 0, 1, out_size, output_full, output_full_post);

    // 2 --> 3
    add_connection(base, sender, 2, 3, 1, 0);

    // 3 --> 4
    //br_empty_1
    add_connection(base, sender, 3, 4, 0, 1, br_size, broadcast_empty, broadcast_empty_post);

    //  4 --> 5
    add_connection(base, sender, 4, 5, 1, 0);

    //    5 --> 6
    //out_lock_1
    add_connection(base, sender, 5, 6, 0, 1, 1, output_lock, output_lock_post);

    //    6 --> 7
    add_connection(base, sender, 6, 7, 1, 0);

    //    7 --> 8
    //br_lock_1
    add_connection(base, sender, 7, 8, 0, 1, 1, broadcast_lock, broadcast_lock_post);

    //    8 --> 9
    //out_lock_2
    add_connection(base, sender, 8, 9, 1, 0, out_size, output_lock, output_lock_post, true);

    //    9 --> 10
    //br_lock_2
    add_connection(base, sender, 9, 10, 1, 0, 1, broadcast_lock, broadcast_lock_post, true);

    //    10 --> 11
    //br_full_2
    add_connection(base, sender, 10, 11, 1, 0, br_size, broadcast_full, broadcast_full_post, true);

    //    11 --> 0
    //out_empty_2
    add_connection(base, sender, 11, 0, 1, 0, out_size, output_empty, output_empty_post, true);

    //  0 --> 12
    add_connection(base, sender, 0, 12, 1, 0);

    //12 --> 13
    //out_full_1
    add_connection(base, sender, 12, 13, 0, 1, out_size, output_full, output_full_post);

    // 13 --> 14
    add_connection(base, sender, 13, 14, 1, 0);

    // 14 --> 15
    //out_lock_1
    add_connection(base, sender, 14, 15, 0, 1, 1, output_lock, output_lock_post);

    //  15 --> 16
    //out_lock_2
    add_connection(base, sender, 15, 16, 1, 0, 1, output_lock, output_lock_post, true);

    //    16 --> 0
    //out_empty_2
    add_connection(base, sender, 16, 0, 1, 0, out_size, output_empty, output_empty_post, true);

    cout << "DONE sender\n";

    /////////////////////////////////

    std::vector<thread<UBDD> *> thread_list;

    thread_list.push_back(&router);
    thread_list.push_back(&delay);
    thread_list.push_back(&sender);

    // cout<<"PRINTING STATES\n";
    // print_bdd_info(router.transitions,router.state_vars,router.state_vars_post);

    // std::vector<uint32_t> tempr_pre = resource_vars;
    // std::vector<uint32_t> tempr_post = resource_vars_post;

    // for(int i=0;i<router.state_vars.size();i++){
    //     tempr_pre.push_back(router.state_vars[i]);
    //     tempr_post.push_back(router.state_vars_post[i]);
    // }

    // cout<<"STORING EVERYTHING\n";

    // store_bdd_in_file(router.transitions,tempr_pre,tempr_post,"../output/router2.txt");

    // cout<<"PRINTING STATES\n";
    // print_bdd_info(delay.transitions,delay.state_vars,delay.state_vars_post);

    // std::vector<uint32_t> tempd_pre = resource_vars;
    // std::vector<uint32_t> tempd_post = resource_vars_post;

    // for(int i=0;i<delay.state_vars.size();i++){
    //     tempd_pre.push_back(delay.state_vars[i]);
    //     tempd_post.push_back(delay.state_vars_post[i]);
    // }

    // cout<<"STORING EVERYTHING\n";

    // store_bdd_in_file(delay.transitions,tempd_pre,tempd_post,"../output/delay2.txt");


    // cout<<"PRINTING STATES\n";
    // print_bdd_info(sender.transitions,sender.state_vars,sender.state_vars_post);

    // std::vector<uint32_t> temps_pre = resource_vars;
    // std::vector<uint32_t> temps_post = resource_vars_post;

    // for(int i=0;i<sender.state_vars.size();i++){
    //     temps_pre.push_back(sender.state_vars[i]);
    //     temps_post.push_back(sender.state_vars_post[i]);
    // }

    //cout<<"STORING EVERYTHING\n";

    // store_bdd_in_file(sender.transitions,temps_pre,temps_post,"../output/sender2.txt");

    std::set<size_t> all_variables;

    /* variables on which the transitions depend but are abstracted out in the product */
    std::vector<size_t> other_variables = input_action_vars;
    for (size_t i = 0; i < output_action_vars.size(); i++) {
        other_variables.push_back(output_action_vars[i]);
    }

    cout << "ARENA BUILDING\n";

    /* Build the arena */

    std::map<std::string, UBDD> progress_pred_to_bdd; /* this is a map that will be used in the rabin automaton for the specification */
    Arena<UBDD> A(base,
                  all_variables,
                  thread_list,
                  resource_vars,
                  resource_states,
                  resource_vars_post,
                  input_action_vars,
                  output_action_vars,
                  input_action_states,
                  output_action_states,
                  progress_pred_to_bdd);
    RabinAutomaton<UBDD> R(base,
                           all_variables,
                           //                     A.nodes_,
                           progress_pred_to_bdd,
                           "../examples/lego/rabin_3_hoa.txt");

    //cout<<"ARENA DONE\n";

    fairsyn::Fixpoint<UBDD> fp(base, "under", &A, &R, other_variables);

    TicToc timer;
    timer.tic();

    UBDD controller;
    if (parallel)
        controller = run_rabin(fp, M, true);
    else
        controller = run_rabin(fp, M, false);
    double synt_time = timer.toc();

    logfile.open(log_filename, std::fstream::app);

    logfile << "Time: " << synt_time << "  ";

    std::vector<size_t> temp = fp.preVars_;
    for (int i = 0; i < fp.postVars_.size(); i++) {
        temp.push_back(fp.postVars_[i]);
    }
    sort(temp.begin(), temp.end());


    logfile << "number_of_states: " << fp.tr_.existAbstract(fp.cubePost_ * fp.cubeOther_).countMinterm(fp.preVars_.size()) << "  ";
    logfile << "number_of_tot_states: " << fp.nodes_.countMinterm(fp.preVars_.size()) << "  ";
    logfile << "number_of_sys_states: " << (fp.sys_nodes_ * fp.tr_.existAbstract(fp.cubePost_)).existAbstract(fp.cubeOther_).countMinterm(fp.preVars_.size()) << "  ";
    logfile << "number_of_env_states: " << (fp.env_nodes_ * fp.tr_.existAbstract(fp.cubePost_)).existAbstract(fp.cubeOther_).countMinterm(fp.preVars_.size()) << "  ";
    logfile << "number_of_transitions: " << fp.tr_.existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "  ";
    logfile << "number_of_env_transitions: " << (fp.env_nodes_ * fp.tr_).existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "  ";
    logfile << "number_of_live_transitions: " << (fp.env_nodes_ & fp.tr_).existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "  ";
    logfile << "number_of_state_variables: " << fp.preVars_.size() << "  ";
    logfile << "Mode: " << is_sylvan << "  ";
    logfile << "Time: " << synt_time << "\n";

    logfile.close();

    assert((fp.sys_nodes_ & fp.env_nodes_) == base.zero());

    cout << "number_of_states : " << fp.tr_.existAbstract(fp.cubePost_ * fp.cubeOther_).countMinterm(fp.preVars_.size()) << "\n";
    cout << "number_of_tot_states : " << fp.nodes_.countMinterm(fp.preVars_.size()) << "\n";
    cout << "number_of_sys_states: " << (fp.sys_nodes_ * fp.tr_.existAbstract(fp.cubePost_)).existAbstract(fp.cubeOther_).countMinterm(fp.preVars_.size()) << "\n";
    cout << "number_of_env_states: " << (fp.env_nodes_ * fp.tr_.existAbstract(fp.cubePost_)).existAbstract(fp.cubeOther_).countMinterm(fp.preVars_.size()) << "\n";
    cout << "number_of_transitions: " << fp.tr_.existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "\n";
    cout << "number_of_env_transitions: " << (fp.env_nodes_ * fp.tr_).existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "\n";
    cout << "number_of_live_transitions: " << (fp.env_nodes_ & fp.tr_).existAbstract(fp.cubeOther_).countMinterm(temp.size()) << "\n";
    cout << "Mode: " << is_sylvan << "\n";
    cout << "Time: " << synt_time << "\n";


    //     cout<<"\n";
    //     UBDD win = controller & fp.sys_nodes_;
    //    cout<<"prevars_variables :: \t " << fp.preVars_.size()<<"\n";
    //     cout<<"system nodes :: \t " <<fp.sys_nodes_.existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"environment nodes :: \t "<<fp.env_nodes_.existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"winning system nodes :: \t " <<(fp.sys_nodes_&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"winning environment nodes :: \t "<<(fp.env_nodes_&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"winning nodes with br_full == 1 && out_full == 1 && router_satte == 0 && sender_state == 0 && delay_state == 0 && br_empty == 0 && out_lock == 0 &&   :: \t "<<( helper::elementToBdd<UBDD>(base,0, sender.state_vars), sender.state_vars.size()) & helper::elementToBdd<UBDD>(base,0, router_vars_bdd, router.state_vars.size()) & helper::elementToBdd<UBDD>(base,0, delay_vars_post_bdd, delay.state_vars.size()) & (fp.sys_nodes_)&(!sylvan::Bdd::bddVar(resource_vars[1]))&(!sylvan::Bdd::bddVar(resource_vars[4]))&(sylvan::Bdd::bddVar(resource_vars[0]))&(sylvan::Bdd::bddVar(resource_vars[3]))&(sylvan::Bdd::bddVar(resource_vars[2]))&(sylvan::Bdd::bddVar(resource_vars[5]))&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //         UBDD store = helper::elementToBdd<UBDD>(base,0, sender.state_vars), sender.state_vars.size()) & helper::elementToBdd<UBDD>(base,0, router_vars_bdd, router.state_vars.size()) & helper::elementToBdd<UBDD>(base,0, delay_vars_post_bdd, delay.state_vars.size()) & fp.sys_nodes_&sylvan::Bdd::bddVar(resource_vars[1])&sylvan::Bdd::bddVar(resource_vars[4])&controller;
    //         for(int i=0;i<fp.preVars_.size();i++)
    //     	cout<<fp.preVars_[i]<<" ";
    //     cout<<"\n";
    //     cout<<"winning nodes with br_full == 0 && out_full == 1 :: \t "<<(fp.sys_nodes_&(!sylvan::Bdd::bddVar(resource_vars[1]))&sylvan::Bdd::bddVar(resource_vars[4])&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"winning nodes with br_full == 1 && out_full == 0 :: \t "<<(fp.sys_nodes_&sylvan::Bdd::bddVar(resource_vars[1])&(!sylvan::Bdd::bddVar(resource_vars[4]))&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
    //     cout<<"winning nodes with br_full == 0 && out_full == 0 :: \t "<<(fp.sys_nodes_&(!sylvan::Bdd::bddVar(resource_vars[1]))&(!sylvan::Bdd::bddVar(resource_vars[4]))&controller).existAbstract(fp.cubePost_*fp.cubeOther_).countMinterm(fp.preVars_.size())<<"\n";
}

/*command line input includes
    1.) broadcast queue size
    2.) output queue size
    3.) option store in 0 or 1 file
    4.) value of M
    5.) option of diffrent version of UBDD
        0) CuddUBDD
        1) SylvanUBDD with sequential rabin
        2) SylvanUBDD wit parallel rabin
*/

int main(int argc, char *argv[]) {

    size_t is_sylvan = str2int(argv[5]);
    std::string log_filename = (str2int(argv[3])) ? "general_1.txt" : "general_0.txt";

    if (is_sylvan) {
        /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
        // int n_workers = 0; // 0 for auto-detect
        int dqsize = 10000000;
        lace_start(n_workers, dqsize);
        // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
        sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
        sylvan::sylvan_init_package();
        sylvan::sylvan_init_mtbdd();
        // starting to build the thread interface
        if (is_sylvan > 1)
            run_experiment<SylvanUBDD>(str2int(argv[1]), str2int(argv[2]), log_filename, str2int(argv[4]), true, is_sylvan);
        else
            run_experiment<SylvanUBDD>(str2int(argv[1]), str2int(argv[2]), log_filename, str2int(argv[4]), false, is_sylvan);

        sylvan::sylvan_stats_report(stdout);
        sylvan::sylvan_quit();
        lace_stop();
    } else {
        run_experiment<CuddUBDD>(str2int(argv[1]), str2int(argv[2]), log_filename, str2int(argv[4]), false, is_sylvan);
    }

    return 0;
}