
/* synthesis.cc */

#include "Arena.hh"
#include "Fixpoint.hh"
#include "RabinAutomaton.hh"
#include "lib/ParallelRabinRecurse.hh"
#include "testlib.h"
#include "ubdd/CuddUBDD.hh"
#include "ubdd/SylvanUBDD.hh"
#include "utils/TicToc.hh"
#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/resource.h>
#include <sys/time.h>
#include <utility>// for the class called pair

using namespace fairsyn;

/*

Command line arguments are as follows :

1. 1. slow -- 0 
   2. fast -- 1 

2. 1. take a text file as input which contains the graph info -- 0
   2. run a small example from the paper for testing  -- 1 

3. verbose information 
   1. print nothing -- 0 
   2. print only number of states -- 1
   3. print all the states in the current iteration -- 2 

4. number of threads // parallel workers -- 
    can be anything from 0 (auto detection !! not effective always ) to 48 (48 for brain cluster) 

5. 1. non accelerated -- 0 
   2. accelerated -- 1

6. parameter M ---

8. specify the number of rabin pairs 

7. specify the number of live edges 
   1. randomly selecting number of live edges  :: -1 
   2. specifying the number of live edges 

8. Random seed to set the seed 

9. complete location to save all important informations  

10. complete location to save the final controller strategy in the same format as input file

11. complete location for the input file  . txt

12. specify the UBDD type:
    0. UBDDCudd
    1. UBDDSylvan
    1. UBDDSylvan with ParallelRabinRecurse

*/

/* example for a command line argument with parameters */
// 1 1 0 0 1 10 3 -1 1 info.txt controller.txt ../bcg/output/1.txt

int main(int argc, char *argv[]) {

    bool fast, run_example, accelerated;
    int verbose, seed, n_workers, M, num_of_rabin_pairs, num_of_live_edges, num_of_nodes, num_of_transitions, num_of_sys_nodes, num_of_env_nodes, is_sylvaubdd;
    std::string imp_info_file, contoller_info_file, input_info_file;
    std::fstream newfile, logfile;

    std::vector<size_t> nodes, sys_nodes, env_nodes;
    std::vector<std::vector<size_t>> transitions, live_edges;
    std::vector<std::array<std::vector<size_t>, 2>> RabinPairs;

    cout << "TAKING INPUT FROM COMMAND LINE ARGUMENTS\n";

    fast = atoi(argv[1]);

    run_example = atoi(argv[2]);

    verbose = atoi(argv[3]);

    n_workers = atoi(argv[4]);

    accelerated = (bool)atoi(argv[5]);

    M = atoi(argv[6]);

    num_of_rabin_pairs = atoi(argv[7]);

    num_of_live_edges = atoi(argv[8]);

    seed = atoi(argv[9]);

    imp_info_file = argv[10];

    contoller_info_file = argv[11];

    input_info_file = argv[12];

    is_sylvaubdd = atoi(argv[13]);

    cout << "INITIALISING REGISTER GENERATOR\n";

    registerGen((long long int)seed, 1);

    if (is_sylvaubdd) {
        cout << "SYLVAN INIT START \n ";
        /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
        int dqsize = 10000000;
        lace_start(n_workers, dqsize);
        // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
        sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
        sylvan::sylvan_init_package();
        sylvan::sylvan_init_mtbdd();

        cout << "SYLVAN INIT ENDS\n";
    }

    if (run_example) {

        cout << "EXAMPLE INIT STARTS\n";

        // example present in the paper with explaination

        num_of_nodes = 7;
        num_of_sys_nodes = 3;
        num_of_env_nodes = 4;
        num_of_rabin_pairs = 2;
        num_of_live_edges = 1;
        num_of_transitions = 14;

        nodes = {0, 1, 2, 3, 4, 5, 6};
        sys_nodes = {4, 5, 6};
        env_nodes = {0, 1, 2, 3};
        transitions = {
                {0, 1},
                {1, 2, 4},
                {2, 5},
                {3, 2},
                {0, 2},
                {1, 6},
                {3}};

        live_edges = {
                {},
                {2},
                {},
                {},
                {},
                {},
                {}};

        std::vector<size_t> G = {0, 3};
        std::vector<size_t> R = {1, 4};
        std::array<std::vector<size_t>, 2> p = {G, R};
        RabinPairs = {p};
        G = {2};
        R = {0, 3, 6};
        p = {G, R};
        RabinPairs.push_back(p);

    } else {


        num_of_transitions = 0;

        cout << "TAKING INPUT FROM FILE\n";

        newfile.open(input_info_file, std::fstream::in);

        // the input file should contain the number of nodes in the 1st line

        // the (i+1)th line contains the nodes which are attached to the ith node  and each line ends with a -1

        if (!newfile) {
            std::cout << "Unable to open file.\n";
            exit(0);
        }

        newfile >> num_of_nodes;

        for (int i = 0; i < num_of_nodes; i++)
            nodes.push_back(i);

        std::vector<size_t> temp_nodes;

        for (int i = 0; i < num_of_nodes; i++)
            temp_nodes.push_back(i);


        shuffle(temp_nodes.begin(), temp_nodes.end());

        // randomly distributing 50% of nodes as system nodes and the rest as environment nodes

        num_of_sys_nodes = num_of_nodes / 2;

        for (int i = 0; i < num_of_sys_nodes; i++)
            sys_nodes.push_back(temp_nodes[i]);

        num_of_env_nodes = num_of_nodes - num_of_sys_nodes;

        std::vector<bool> is_env_node(num_of_nodes, false);

        for (int i = num_of_sys_nodes; i < num_of_nodes; i++) {
            env_nodes.push_back(temp_nodes[i]);
            is_env_node[temp_nodes[i]] = true;
        }


        int num_of_env_transitions = 0;

        // determining the number of transitions from environment nodes so that we can assign them as live edges

        std::vector<std::pair<int, int>> env_transitions;

        for (int i = 0; i < num_of_nodes; i++) {

            int num;
            std::vector<size_t> single_node_transitions;
            newfile >> num;

            while (num != -1) {
                single_node_transitions.push_back(num);
                ++num_of_transitions;
                if (is_env_node[i]) {
                    ++num_of_env_transitions;
                    env_transitions.push_back({i, num});
                }
                newfile >> num;
            }

            transitions.push_back(single_node_transitions);
        }

        newfile.close();

        cout << "INPUT TAKEN\n";

        cout << "RABIN PAIRS ASSIGNMENT STARTED\n";

        // the maximum numbe of nodes in a single Rabin pair condition

        int upper_bound_on_nodes = num_of_nodes / 20;

        // randomly assigning rabin pairs

        for (int i = 0; i < num_of_rabin_pairs; i++) {

            std::vector<size_t> G;
            std::vector<size_t> R;

            std::array<std::vector<size_t>, 2> p;

            // G VECTOR

            int ver_G = rnd.next(1, upper_bound_on_nodes);

            temp_nodes.clear();
            for (int i = 0; i < num_of_nodes; i++)
                temp_nodes.push_back(i);

            shuffle(temp_nodes.begin(), temp_nodes.end());

            for (int i = 0; i < ver_G; i++) {
                G.push_back(temp_nodes[i]);
            }

            //R VECTOR

            int ver_R = rnd.next(1, upper_bound_on_nodes);

            temp_nodes.clear();
            for (int i = 0; i < num_of_nodes; i++)
                temp_nodes.push_back(i);

            shuffle(temp_nodes.begin(), temp_nodes.end());

            for (int i = 0; i < ver_R; i++) {
                R.push_back(temp_nodes[i]);
            }

            p = {G, R};

            RabinPairs.push_back(p);
        }

        cout << "RABIN PAIRS PROCESS ENDS\n";

        cout << "LIVE EDGES ASSIGN STARTS\n";

        if (num_of_live_edges == -1) {

            //randomly determining the number of live edges

            int upper_bound_on_live_edges = num_of_env_transitions / 20;

            num_of_live_edges = rnd.next(1, upper_bound_on_live_edges);
        }

        shuffle(env_transitions.begin(), env_transitions.end());

        for (int i = 0; i < num_of_nodes; i++) {
            std::vector<size_t> temp;
            live_edges.push_back(temp);
        }

        cout << env_transitions[0].first << " " << env_transitions[0].second << "\n";

        if (fast) {

            for (int i = 0; i < num_of_live_edges; i++)
                live_edges[env_transitions[i].first].push_back(env_transitions[i].second);

        } else {

            // if slow algo then converting each live edges to a rabin pair conditon by adding an extra node by breaking th edge

            for (int i = 0; i < num_of_live_edges; i++) {

                std::vector<size_t> temp;
                live_edges.push_back(temp);
                transitions.push_back(temp);
                env_nodes.push_back(nodes.size());
                nodes.push_back(nodes.size());
            }

            for (int i = 0; i < num_of_live_edges; i++) {

                int src = env_transitions[i].first;
                int sink = env_transitions[i].second;
                std::vector<size_t> temp;
                for (int i = 0; i < transitions[src].size(); i++) {
                    if (transitions[src][i] == sink)
                        continue;
                    temp.push_back(transitions[src][i]);
                }
                transitions[src] = temp;
                transitions[src].push_back(num_of_nodes + i);
                transitions[num_of_nodes + i].push_back(sink);
                ++num_of_transitions;
                ++num_of_env_transitions;
                ++num_of_rabin_pairs;
                std::vector<size_t> G = {(size_t)src};
                std::vector<size_t> R = {(size_t)(num_of_nodes + i)};
                std::array<std::vector<size_t>, 2> p = {G, R};
                RabinPairs.push_back(p);
            }

            num_of_nodes += num_of_live_edges;
            num_of_env_nodes += num_of_live_edges;
        }

        cout << "LIVE EDGES PROCESS ENDS\n";
    }

    cout << "INITIALISATION DONE\n";

    /*
        OUTPUT all initial important information 
        1. file name 
        2. seed used to initialise 
        3. number of nodes 
        4. number of edges 
        5. number of rabin pairs 
        6. number of live edges 
        7. value of M 
        8. number of threads 
    */


    logfile.open(imp_info_file, std::fstream::app);

    logfile << "\n";

    logfile << input_info_file << " " << seed << " " << num_of_nodes << " " << num_of_transitions << " "
            << num_of_rabin_pairs << " " << num_of_live_edges << " " << M << " " << n_workers << " ";

    logfile.close();

    std::set<size_t> all_variables;
    double ans;
    double final_ans;
    if (is_sylvaubdd) {
        SylvanUBDD base;
        cout << "ARENA START\n";
        Fixpoint<SylvanUBDD> Fp(base, "under", nodes, sys_nodes, env_nodes, transitions, live_edges, RabinPairs, all_variables);
        cout << "ARENA END\n";
        TicToc total_time;
        total_time.tic();
        cout << "CONTROLLER START\n";
        cout << accelerated << " " << M << " " << verbose << "\n";
        SylvanUBDD controller_strategy;
        if (is_sylvaubdd > 1)
            controller_strategy = Fp.RabinEnd(Fp.Rabin(accelerated, M, Fp.nodes_, verbose, genie::ParallelRabinRecurse));
        else
            controller_strategy = Fp.RabinEnd(Fp.Rabin(accelerated, M, Fp.nodes_, verbose));
        cout << "CONTROLLER ENDS\n";
        ans = total_time.toc();
        final_ans = ans;
        struct rusage usage;
        int rc = getrusage(RUSAGE_SELF, &usage);
        /*
        OUTPUT all important information after controller synthesis
        1. number of BDD variables
        2. time taken
        3. memory 
        */
        logfile.open(imp_info_file, std::fstream::app);
        logfile << Fp.preVars_.size() << " " << final_ans << " " << usage.ru_maxrss;
        logfile.close();
        std::vector<bool> is_sys_node(num_of_nodes, false);
        for (int i = 0; i < sys_nodes.size(); i++)
            is_sys_node[sys_nodes[i]] = true;
        helper::winning_region_file(base, controller_strategy, Fp.preVars_, contoller_info_file, is_sys_node);
        helper::controller_file(base, controller_strategy, Fp.preVars_, Fp.postVars_, transitions, contoller_info_file, is_sys_node);
        /* some sylvan statistics */
        sylvan::sylvan_stats_report(stdout);
        sylvan::sylvan_quit();
        lace_stop();
    } else {
        CuddUBDD base;
        cout << "ARENA START\n";
        Fixpoint<CuddUBDD> Fp(base, "under", nodes, sys_nodes, env_nodes, transitions, live_edges, RabinPairs, all_variables);
        cout << "ARENA END\n";
        TicToc total_time;
        total_time.tic();
        cout << "CONTROLLER START\n";
        cout << accelerated << " " << M << " " << verbose << "\n";
        CuddUBDD controller_strategy;
        controller_strategy = Fp.RabinEnd(Fp.Rabin(accelerated, M, Fp.nodes_, verbose));
        cout << "CONTROLLER ENDS\n";
        ans = total_time.toc();
        final_ans = ans;
        struct rusage usage;
        int rc = getrusage(RUSAGE_SELF, &usage);
        /*
        OUTPUT all important information after controller synthesis
        1. number of BDD variables
        2. time taken
        3. memory
        */
        logfile.open(imp_info_file, std::fstream::app);
        logfile << Fp.preVars_.size() << " " << final_ans << " " << usage.ru_maxrss;
        logfile.close();
        std::vector<bool> is_sys_node(num_of_nodes, false);
        for (int i = 0; i < sys_nodes.size(); i++)
            is_sys_node[sys_nodes[i]] = true;
        helper::winning_region_file(base, controller_strategy, Fp.preVars_, contoller_info_file, is_sys_node);
        helper::controller_file(base, controller_strategy, Fp.preVars_, Fp.postVars_, transitions, contoller_info_file, is_sys_node);
    }

    return 0;
}
