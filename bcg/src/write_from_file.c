#include "bcg_user.h"

int main(int argc, char *argv[]) {
    BCG_TYPE_STATE_NUMBER S1;
    BCG_TYPE_LABEL_STRING L = "";
    BCG_TYPE_STATE_NUMBER S2;
    BCG_INIT();
    char *file_name_give = argv[1];
    BCG_IO_WRITE_BCG_BEGIN(file_name_give, 6, 2, "created by tool", 1);

    char *file_name_open = argv[2];

    FILE *fptr;

    fptr = fopen(file_name_open, "r");

    int n;

    fscanf(fptr, "%d", &n);

    for (int i = 0; i < n; i++) {
        int num;
        fscanf(fptr, "%d", &num);
        S1 = i;
        while (num != -1) {
            S2 = num;
            BCG_IO_WRITE_BCG_EDGE(S1, L, S2);
            printf("%d %d \n", i, num);
            fscanf(fptr, "%d", &num);
        }
    }

    fclose(fptr);
    BCG_IO_WRITE_BCG_END();

    return 0;
}

