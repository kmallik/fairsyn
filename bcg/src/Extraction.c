#include "bcg_user.h"

/* The following function prints information about a BCG graph */
void bcg_print_info(BCG_TYPE_OBJECT_TRANSITION bcg_graph) {
    printf("initial state = %lu\n", BCG_OT_INITIAL_STATE(bcg_graph));
    printf("nb states = %lu\n", BCG_OT_NB_STATES(bcg_graph));
    printf("nb edges = %lu\n", BCG_OT_NB_EDGES(bcg_graph));
    printf("nb labels = %u\n", BCG_OT_NB_LABELS(bcg_graph));
}

/* The following function displays an edge */

void bcg_print_edge(BCG_TYPE_OBJECT_TRANSITION bcg_graph, BCG_TYPE_STATE_NUMBER bcg_state_1,
                    BCG_TYPE_LABEL_NUMBER bcg_label_number, BCG_TYPE_STATE_NUMBER bcg_state_2) {
    BCG_TYPE_C_STRING bcg_label_string;
    BCG_TYPE_BOOLEAN bcg_visible;
    BCG_TYPE_NATURAL bcg_cardinal;
    BCG_TYPE_C_STRING bcg_gate;

    bcg_label_string = BCG_OT_LABEL_STRING(bcg_graph, bcg_label_number);
    bcg_visible = BCG_OT_LABEL_VISIBLE(bcg_graph, bcg_label_number);
    bcg_cardinal = BCG_OT_LABEL_CARDINAL(bcg_graph, bcg_label_number);

    fprintf("transition from state %lu to state %lu\n",
            bcg_state_1, bcg_state_2);
    /*printf ("label unique number = %u\n", bcg_label_number);
    printf ("label string = %s\n", bcg_label_string);
    printf ("label cardinal = %u\n", bcg_cardinal);
    if (bcg_visible) {
       bcg_gate = BCG_OT_LABEL_GATE (bcg_graph, bcg_label_number);
       printf ("visible label (gate = %s)\n", bcg_gate);
    } else {
       bcg_gate = BCG_OT_LABEL_HIDDEN_GATE (bcg_graph, bcg_label_number);
       printf ("hidden label (hidden gate = %s)\n", bcg_gate);
    }*/
}


int main(int argc, char *argv[]) {
    FILE *fptr;
    char *file_path1 = "../output/";
    char *file_path2 = "../input/";
    char *file_extention1 = ".txt";
    char *file_extention2 = ".bcg";

    int size_fileopen = strlen(argv[1]) + strlen(file_path1) + strlen(file_extention1) + 1;
    char *fileopen = (char *) malloc(size_fileopen);
    strcpy(fileopen, file_path1);
    strcat(fileopen, argv[1]);
    strcat(fileopen, file_extention1);
    fptr = fopen(fileopen, "w");

    BCG_TYPE_OBJECT_TRANSITION bcg_graph;
    BCG_TYPE_STATE_NUMBER bcg_s1;
    BCG_TYPE_LABEL_NUMBER bcg_label_number;
    BCG_TYPE_STATE_NUMBER bcg_s2;
    BCG_TYPE_STATE_NUMBER bcg_nb_states;


    int size_bcgopen = strlen(argv[1]) + strlen(file_path2) + strlen(file_extention2) + 1;
    char *bcgopen = (char *) malloc(size_bcgopen);
    strcpy(bcgopen, file_path2);
    strcat(bcgopen, argv[1]);
    strcat(bcgopen, file_extention2);

    BCG_INIT();
    BCG_OT_READ_BCG_BEGIN(bcgopen, &bcg_graph, 1);
    int initial_state = BCG_OT_INITIAL_STATE(bcg_graph);
    int number_of_states = BCG_OT_NB_STATES(bcg_graph);
    int number_of_edges = BCG_OT_NB_EDGES(bcg_graph);

    fprintf(fptr, "%lu\n", number_of_states);

    /*
    fprintf (fptr,"initial state = %lu\n", BCG_OT_INITIAL_STATE (bcg_graph));
    fprintf (fptr,"nb states = %lu\n", BCG_OT_NB_STATES (bcg_graph));
    fprintf (fptr,"nb edges = %lu\n", BCG_OT_NB_EDGES (bcg_graph));
    fprintf (fptr,"nb labels = %u\n", BCG_OT_NB_LABELS (bcg_graph));
    */

    bcg_nb_states = BCG_OT_NB_STATES(bcg_graph);
    int last = -1;
    for (bcg_s1 = 0; bcg_s1 < bcg_nb_states; bcg_s1++) {
        /*fprintf (fptr,"successors of state %lu:\n", bcg_s1);*/

        BCG_OT_ITERATE_P_LN(bcg_graph, bcg_s1, bcg_label_number, bcg_s2)
        {
            fprintf(fptr, "%lu ", bcg_s2);
        }
        BCG_OT_END_ITERATE;
        fprintf(fptr, "%d\n", last);
    }
    BCG_OT_READ_BCG_END(&bcg_graph);
    fclose(fptr);
    exit(0);
}
