#include "bcg_user.h"

int main() {
    BCG_TYPE_STATE_NUMBER S1;
    BCG_TYPE_LABEL_STRING L = "";
    BCG_TYPE_STATE_NUMBER S2;
    BCG_INIT();
    BCG_IO_WRITE_BCG_BEGIN("test.bcg", 0, 2, "created by tool", 1);
    for (int i = 0; i < 10; i++) {
        S1 = i;
        for (int j = i + 1; j < 10; j++) {
            S2 = j;
            BCG_IO_WRITE_BCG_EDGE(S1, L, S2);
        }
    }
    BCG_IO_WRITE_BCG_END();
    return (0);
}
