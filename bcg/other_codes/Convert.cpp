#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    fstream newfile;
    //cout<<"hi\n";
    newfile.open("cwi_1_2.txt", ios::in);
    int n;
    newfile >> n;
    cout << n << "\n";
    while (n--) {
        int num;
        newfile >> num;
        while (num != -1) {
            cout << num << " ";
            newfile >> num;
        }
        cout << "\n";
    }
    newfile.close();
    return 0;
}
