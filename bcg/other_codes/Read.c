#include "bcg_user.h"

/* The following function prints information about a BCG graph */
void bcg_print_info(BCG_TYPE_OBJECT_TRANSITION bcg_graph) {
    printf("initial state = %lu\n", BCG_OT_INITIAL_STATE(bcg_graph));
    printf("nb states = %lu\n", BCG_OT_NB_STATES(bcg_graph));
    printf("nb edges = %lu\n", BCG_OT_NB_EDGES(bcg_graph));
    printf("nb labels = %u\n", BCG_OT_NB_LABELS(bcg_graph));
}

/* The following function displays an edge */

void bcg_print_edge(BCG_TYPE_OBJECT_TRANSITION bcg_graph, BCG_TYPE_STATE_NUMBER bcg_state_1,
                    BCG_TYPE_LABEL_NUMBER bcg_label_number, BCG_TYPE_STATE_NUMBER bcg_state_2) {
    BCG_TYPE_C_STRING bcg_label_string;
    BCG_TYPE_BOOLEAN bcg_visible;
    BCG_TYPE_NATURAL bcg_cardinal;
    BCG_TYPE_C_STRING bcg_gate;

    bcg_label_string = BCG_OT_LABEL_STRING(bcg_graph, bcg_label_number);
    bcg_visible = BCG_OT_LABEL_VISIBLE(bcg_graph, bcg_label_number);
    bcg_cardinal = BCG_OT_LABEL_CARDINAL(bcg_graph, bcg_label_number);

    printf("transition from state %lu to state %lu\n",
           bcg_state_1, bcg_state_2);
    printf("label unique number = %u\n", bcg_label_number);
    printf("label string = %s\n", bcg_label_string);
    printf("label cardinal = %u\n", bcg_cardinal);
    if (bcg_visible) {
        bcg_gate = BCG_OT_LABEL_GATE(bcg_graph, bcg_label_number);
        printf("visible label (gate = %s)\n", bcg_gate);
    } else {
        bcg_gate = BCG_OT_LABEL_HIDDEN_GATE(bcg_graph, bcg_label_number);
        printf("hidden label (hidden gate = %s)\n", bcg_gate);
    }
}


int main() {
    BCG_TYPE_OBJECT_TRANSITION bcg_graph;
    BCG_TYPE_STATE_NUMBER bcg_s1;
    BCG_TYPE_LABEL_NUMBER bcg_label_number;
    BCG_TYPE_STATE_NUMBER bcg_s2;
    BCG_TYPE_STATE_NUMBER bcg_nb_states;

    BCG_INIT();


    BCG_OT_READ_BCG_BEGIN("vasy_0_1.bcg", &bcg_graph, 0);
    bcg_print_info(bcg_graph);
    BCG_OT_ITERATE_PLN(bcg_graph, bcg_s1, bcg_label_number, bcg_s2)
    {
        bcg_print_edge(bcg_graph, bcg_s1, bcg_label_number, bcg_s2);
    }
    BCG_OT_END_ITERATE;

    BCG_OT_READ_BCG_END(&bcg_graph);
    BCG_OT_READ_BCG_BEGIN("vasy_0_1.bcg", &bcg_graph, 1);
    bcg_print_info(bcg_graph);
    bcg_nb_states = BCG_OT_NB_STATES(bcg_graph);
    for (bcg_s1 = 0; bcg_s1 < bcg_nb_states; bcg_s1++) {
        printf("successors of state %lu:\n", bcg_s1);
        BCG_OT_ITERATE_P_LN(bcg_graph, bcg_s1, bcg_label_number, bcg_s2)
        {
            bcg_print_edge(bcg_graph, bcg_s1, bcg_label_number, bcg_s2);
        }
        BCG_OT_END_ITERATE;
    }
    BCG_OT_READ_BCG_END(&bcg_graph);
    exit(0);
}
