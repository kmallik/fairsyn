/* The Lego example from the code-aware resource management paper */
#include "lib/Arena.hh"
#include "lib/FixedPoint.cpp"
#include "lib/RabinAutomaton.hh"
#include "testlib.h"
#include "TicToc.hh"


#include <chrono>
#include <random>
#include <sstream>
#include <fstream>
#include <iostream>
#include <set>
#include <utility> // for the class called pair
#include <algorithm> // for random shuffle

#define accl_on 1
#define M 5
#define verbose 1
#define n_workers 48


using namespace synthesis;


int str2int(string temp) {
    string s = temp;
    if (s[0] == '-') {
        return str2int(s.substr(1)) * -1;
    }
    std::stringstream ss(s);
    int x = 0;
    ss >> x;
    return x;
}

/*command line input includes
1.)file name ,
 2.)an upper bound on the number of rabin pairs ,
 3.)a number using which we divide the number of nodes
to get the upper bound on the number of vertices in a rabin condition
ie (1/x) * N where x is the command line input and N is the number of nodes
4.) another same for the live edges
*/
int main(int argc, char *argv[]) {


    /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
    // int n_workers = 0; // 0 for auto-detect
    int dqsize = 100000;
    lace_start(n_workers, dqsize);
    // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
    sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
    sylvan::sylvan_init_package();
    sylvan::sylvan_init_mtbdd();

//    sylvan::Bdd a = sylvan::Bdd::bddVar(0);
//    std::cout << "index of a = " << a.TopVar() << "\n";
//    sylvan::Bdd b = sylvan::Bdd::bddVar(0);
//    std::cout << "index of b = " << b.TopVar() << "\n";
//    a = sylvan::Bdd::bddZero();
//    b = sylvan::Bdd::bddZero();
//    assert(a==b);
//    sylvan::Bdd c = a|b;
//    std::cout << "number of nodes in a= " << a.NodeCount() << "\n";
//    std::cout << "number of nodes in b= " << b.NodeCount() << "\n";


    // starting to build the thread interface

    uint32_t cur_vars = 0;

    ///////////////////////////

    std::vector<uint32_t> resource_vars;
    std::vector<uint32_t> resource_vars_post;
    std::vector<uint32_t> input_action_vars;
    std::vector<uint32_t> output_action_vars;

    // resource vars -- 12

    for (int i = 0; i < 12; i++) {
        resource_vars.push_back(cur_vars);
        ++cur_vars;
    }

    for (int i = 0; i < 12; i++) {
        resource_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet resource_vars_bdd = sylvan::BddSet::fromVector(resource_vars);
    sylvan::BddSet resource_vars_post_bdd = sylvan::BddSet::fromVector(resource_vars_post);

    //output actions -- 25

    for (int i = 0; i < 5; i++) {
        output_action_vars.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet output_vars_bdd = sylvan::BddSet::fromVector(output_action_vars);

    //input actions -- 13

    for (int i = 0; i < 4; i++) {
        input_action_vars.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet input_vars_bdd = sylvan::BddSet::fromVector(input_action_vars);

///////////////////////

    sylvan::Bdd resource_states, input_action_states, output_action_states;

    std::vector<uint8_t> resource_state_var_values;
    uint8_t dont_care = 2;
    for (size_t i = 0; i < 12; i++) {
        resource_state_var_values.push_back(dont_care);
    }
    resource_states = sylvan::Bdd::bddCube(resource_vars_bdd, resource_state_var_values);
    // resource_states = sylvan::Bdd::bddZero();
    // for(int i=0;i<12;i++){
    //     resource_states |= sylvan::Bdd::bddVar(resource_vars[i]);
    // }
    input_action_states = sylvan::Bdd::bddZero();
    for (int i = 0; i < 13; i++) {
        input_action_states |= elementToBdd(i, input_vars_bdd, input_action_vars.size());
    }
    output_action_states = sylvan::Bdd::bddZero();
    for (int i = 0; i < 25; i++) {
        output_action_states |= elementToBdd(i, output_vars_bdd, output_action_vars.size());
    }

    // creating generator thread

    // declaring the states

    thread generator;

    for (int i = 0; i < 3; i++) {
        generator.state_vars.push_back(cur_vars);
        ++cur_vars;
    }

    for (int i = 0; i < 3; i++) {
        generator.state_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet generator_vars_bdd = sylvan::BddSet::fromVector(generator.state_vars);
    sylvan::BddSet generator_vars_post_bdd = sylvan::BddSet::fromVector(generator.state_vars_post);

    // generator states

    generator.states = sylvan::Bdd::bddZero();

    for (int i = 0; i < 6; i++) {
        generator.states |= elementToBdd(i, generator_vars_bdd, generator.state_vars.size());
    }

    // transition relations

    //0 -- > 1

    generator.transitions = sylvan::Bdd::bddZero();

    // generate the rules

    //  0 --> 1

    sylvan::Bdd rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(1, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(1, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    generator.transitions |= rule;



    //1 --> 2


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(1, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(2, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(1, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[0]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[0]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 0)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    generator.transitions |= rule;

    // 2 --> 3

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(2, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(3, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(5, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    generator.transitions |= rule;

    // 3 --> 4

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(3, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(4, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(3, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[2]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[2]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    generator.transitions |= rule;

    //  4 --> 5

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(4, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(5, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(6, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[2]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[2]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    generator.transitions |= rule;

//    5 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(5, generator_vars_bdd, generator.state_vars.size());
    rule &= elementToBdd(0, generator_vars_post_bdd, generator.state_vars.size());
    rule &= elementToBdd(4, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[1]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[1]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 1)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    generator.transitions |= rule;

    cout << "DONE Generator\n";

///////////////////////////////////////////////////////////////

    // delay

    // declaring the states

    thread delay;

    for (int i = 0; i < 4; i++) {
        delay.state_vars.push_back(cur_vars);
        ++cur_vars;
    }

    for (int i = 0; i < 4; i++) {
        delay.state_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet delay_vars_bdd = sylvan::BddSet::fromVector(delay.state_vars);
    sylvan::BddSet delay_vars_post_bdd = sylvan::BddSet::fromVector(delay.state_vars_post);

    delay.states = sylvan::Bdd::bddZero();

    for (int i = 0; i < 12; i++) {
        delay.states |= elementToBdd(i, delay_vars_bdd, delay.state_vars.size());
    }

    // transition relations

    delay.transitions = sylvan::Bdd::bddZero();

    // generate the rules

    //  0 --> 1

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(1, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(15, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    delay.transitions |= rule;


    //1 --> 2


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(1, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(2, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(8, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[7]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[7]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 7)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

    // 2 --> 3

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(2, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(3, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(19, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    delay.transitions |= rule;

    // 3 --> 4

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(3, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(4, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(10, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[9]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[9]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 9)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

    //  4 --> 5

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(4, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(5, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(17, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

//    5 --> 6

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(5, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(6, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(9, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[8]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[8]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

//    6 --> 7

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(6, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(7, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(23, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

//    7 --> 8

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(7, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(8, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(12, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[11]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[11]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

//    8 --> 9

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(8, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(9, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(18, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[8]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[8]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

//    9 --> 10

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(9, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(10, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(24, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[11]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[11]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;


//    10 --> 11

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(10, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(11, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(22, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[10]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[10]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 10)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;


//    11 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(11, delay_vars_bdd, delay.state_vars.size());
    rule &= elementToBdd(0, delay_vars_post_bdd, delay.state_vars.size());
    rule &= elementToBdd(14, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[6]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[6]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 6)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    delay.transitions |= rule;

    cout << "DONE delay\n";

//////////////////////////////////////////////////////////

//     // user

//     // declaring the states

//     thread user;

//     for(int i=0;i<4;i++){
//         user.state_vars.push_back(cur_vars);
//         ++cur_vars;
//     }

//     for(int i=0;i<4;i++){
//         user.state_vars_post.push_back(cur_vars);
//         ++cur_vars;
//     }

//     sylvan::BddSet user_vars_bdd = sylvan::BddSet::fromVector(user.state_vars);
//     sylvan::BddSet user_vars_post_bdd = sylvan::BddSet::fromVector(user.state_vars_post);

//     user.states = sylvan::Bdd::bddZero();

//     for(int i=0;i<12;i++){
//         user.states |= elementToBdd(i, user_vars_bdd, user.state_vars.size());
//     }

//     // transition relations

//     user.transitions = sylvan::Bdd::bddZero() ;

//     // generate the rules

//    //  0 --> 1

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(0, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(1, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(9, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }


//     user.transitions |= rule;


//     //1 --> 2


//     rule = sylvan::Bdd::bddOne() ;
//     rule &= elementToBdd(1, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(2, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(5, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[4]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[4]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 4)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

//     // 2 --> 3

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(2, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(3, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(1, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }


//     user.transitions |= rule;

//     // 3 --> 4

//     rule = sylvan::Bdd::bddOne() ;
//     rule &= elementToBdd(3, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(4, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(1, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[0]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[0]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 0)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

//    //  4 --> 5

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(4, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(5, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(11, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

// //    5 --> 6

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(5, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(6, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(6, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[5]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[5]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 5)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

// //    6 --> 7

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(6, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(7, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(5, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

// //    7 --> 8

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(7, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(8, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(3, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[2]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[2]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 2)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

// //    8 --> 9

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(8, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(9, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(12, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[5]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[5]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 5)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

// //    9 --> 10

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(9, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(10, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(6, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[2]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[2]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 2)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;


// //    10 --> 11

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(10, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(11, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(3, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[1]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[1]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 1)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;


// //    11 --> 0

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(11, user_vars_bdd, user.state_vars.size());
//     rule &= elementToBdd(0, user_vars_post_bdd, user.state_vars.size());
//     rule &= elementToBdd(8, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[3]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[3]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 3)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     user.transitions |= rule;

//     cout<<"DONE user\n";

//////////////////////////////////////////////////////////

    // sender

    // declaring the states

    thread sender;

    for (int i = 0; i < 5; i++) {
        sender.state_vars.push_back(cur_vars);
        ++cur_vars;
    }

    for (int i = 0; i < 5; i++) {
        sender.state_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet sender_vars_bdd = sylvan::BddSet::fromVector(sender.state_vars);
    sylvan::BddSet sender_vars_post_bdd = sylvan::BddSet::fromVector(sender.state_vars_post);

    sender.states = sylvan::Bdd::bddZero();

    for (int i = 0; i < 17; i++) {
        sender.states |= elementToBdd(i, sender_vars_bdd, sender.state_vars.size());
    }

    // transition relations

    sender.transitions = sylvan::Bdd::bddZero();

    // generate the rules

    //  0 --> 1

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(1, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(21, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    sender.transitions |= rule;


    //1 --> 2


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(1, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(2, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(11, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[10]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[10]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 10)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    // 2 --> 3

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(2, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(3, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(13, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    sender.transitions |= rule;

    // 3 --> 4

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(3, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(4, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(7, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[6]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[6]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 6)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    //  4 --> 5

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(4, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(5, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(23, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

//    5 --> 6

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(5, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(6, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(12, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[11]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[11]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

//    6 --> 7

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(6, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(7, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(17, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

//    7 --> 8

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(7, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(8, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(9, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[8]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[8]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

//    8 --> 9

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(8, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(9, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(24, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[11]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[11]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

//    9 --> 10

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(9, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(10, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(18, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[8]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[8]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;


//    10 --> 11

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(10, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(11, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(16, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[7]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[7]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 7)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;


//    11 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(11, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(20, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[9]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[9]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 9)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    //  0 --> 12

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(12, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(21, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    sender.transitions |= rule;



    //12 --> 13


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(12, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(13, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(11, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[10]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[10]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 10)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    // 13 --> 14

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(13, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(14, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(23, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    sender.transitions |= rule;

    // 14 --> 15

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(14, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(15, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(12, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[11]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[11]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    //  15 --> 16

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(15, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(16, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(24, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[11]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[11]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    sender.transitions |= rule;

//    16 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(16, sender_vars_bdd, sender.state_vars.size());
    rule &= elementToBdd(0, sender_vars_post_bdd, sender.state_vars.size());
    rule &= elementToBdd(20, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[9]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[9]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 9)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    sender.transitions |= rule;

    cout << "DONE sender\n";


//////////////////////////////////////////////////////////



    // router

    // declaring the states

    thread router;

    for (int i = 0; i < 6; i++) {
        router.state_vars.push_back(cur_vars);
        ++cur_vars;
    }

    for (int i = 0; i < 6; i++) {
        router.state_vars_post.push_back(cur_vars);
        ++cur_vars;
    }

    sylvan::BddSet router_vars_bdd = sylvan::BddSet::fromVector(router.state_vars);
    sylvan::BddSet router_vars_post_bdd = sylvan::BddSet::fromVector(router.state_vars_post);

    router.states = sylvan::Bdd::bddZero();

    for (int i = 0; i < 34; i++) {
        router.states |= elementToBdd(i, router_vars_bdd, router.state_vars.size());
    }

    // transition relations

    router.transitions = sylvan::Bdd::bddZero();

    // generate the rules

//    //  0 --> 1

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(0, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(1, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(3, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }


//     router.transitions |= rule;


//     //1 --> 2


//     rule = sylvan::Bdd::bddOne() ;
//     rule &= elementToBdd(1, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(2, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(2, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[1]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[1]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 1)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

//     // 2 --> 3

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(2, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(3, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(7, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }


//     router.transitions |= rule;

//     // 3 --> 4

//     rule = sylvan::Bdd::bddOne() ;
//     rule &= elementToBdd(3, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(4, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(4, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[3]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[3]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 3)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

//    //  4 --> 5

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(4, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(5, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(5, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //    5 --> 6

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(5, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(6, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(3, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[2]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[2]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 2)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //    6 --> 7

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(6, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(7, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(11, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

//     for(int i=0;i<resource_vars.size();i++){
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //    7 --> 8

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(7, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(8, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(6, input_vars_bdd, input_action_vars.size());
//     rule &= sylvan::Bdd::bddVar(resource_vars[5]);
//     rule &= (!sylvan::Bdd::bddVar(resource_vars_post[5]));

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 5)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //    8 --> 9

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(8, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(9, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(6, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[2]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[2]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 2)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //    9 --> 10

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(9, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(10, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(12, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[5]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[5]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 5)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;


// //    10 --> 11

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(10, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(11, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(11, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[4]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[4]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 4)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;


// //    11 --> 0

//     rule = sylvan::Bdd::bddOne()  ;
//     rule &= elementToBdd(11, router_vars_bdd, router.state_vars.size());
//     rule &= elementToBdd(0, router_vars_post_bdd, router.state_vars.size());
//     rule &= elementToBdd(2, output_vars_bdd, output_action_vars.size());
//     rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
//     rule &= (!sylvan::Bdd::bddVar(resource_vars[0]));
//     rule &= sylvan::Bdd::bddVar(resource_vars_post[0]);

//     for(int i=0;i<resource_vars.size();i++){
//         if( i == 0)
//             continue;
//      sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
//      b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
//      rule &= b;
//     }

//     router.transitions |= rule;

// //////////////////////////////////////////////////////////



    // generate the rules

    //  0 --> 12

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(1, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(4, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    router.transitions |= rule;


    //12 --> 13


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(1, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(2, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(2, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[1]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[1]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 1)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

    // 13 --> 14

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(2, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(3, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(13, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    router.transitions |= rule;

    // 14 --> 15

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(3, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(4, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(7, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[6]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[6]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 6)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

    //  15 --> 16

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(4, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(5, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(5, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    16 --> 17

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(16, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(17, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(3, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[2]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[2]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    17 --> 18

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(17, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(18, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(17, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    18 --> 19

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(18, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(19, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(9, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[8]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[8]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    19 --> 20

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(19, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(20, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(6, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[2]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[2]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    20 --> 21

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(20, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(21, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(18, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[8]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[8]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 8)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;


//    21 --> 22

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(21, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(22, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(16, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[7]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[7]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 7)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;


//    22 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(22, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(0, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(2, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[0]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[0]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 0)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//////////////////////////////////////////////////////////



    // generate the rules

    //  0 --> 23

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(0, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(23, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(4, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    router.transitions |= rule;


    //23 --> 24


    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(23, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(24, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(2, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[1]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[1]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 1)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

    // 24 --> 25

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(24, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(25, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(19, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }


    router.transitions |= rule;

    // 25 --> 26

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(25, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(26, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(10, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[9]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[9]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 9)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

    //  26 --> 27

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(26, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(27, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(5, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    27 --> 28

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(27, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(28, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(3, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[2]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[2]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    28 --> 29

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(28, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(29, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(23, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());

    for (int i = 0; i < resource_vars.size(); i++) {
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    29 --> 30

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(29, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(30, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(0, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(12, input_vars_bdd, input_action_vars.size());
    rule &= sylvan::Bdd::bddVar(resource_vars[11]);
    rule &= (!sylvan::Bdd::bddVar(resource_vars_post[11]));

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    30 --> 31

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(30, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(31, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(6, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[2]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[2]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 2)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

//    31 --> 32

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(31, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(32, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(24, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[11]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[11]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 11)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;


//    32 --> 33

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(32, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(33, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(22, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[10]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[10]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 10)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;


//     33 --> 0

    rule = sylvan::Bdd::bddOne();
    rule &= elementToBdd(33, router_vars_bdd, router.state_vars.size());
    rule &= elementToBdd(0, router_vars_post_bdd, router.state_vars.size());
    rule &= elementToBdd(2, output_vars_bdd, output_action_vars.size());
    rule &= elementToBdd(0, input_vars_bdd, input_action_vars.size());
    rule &= (!sylvan::Bdd::bddVar(resource_vars[0]));
    rule &= sylvan::Bdd::bddVar(resource_vars_post[0]);

    for (int i = 0; i < resource_vars.size(); i++) {
        if (i == 0)
            continue;
        sylvan::Bdd b = sylvan::Bdd::bddVar(resource_vars[i]) & sylvan::Bdd::bddVar(resource_vars_post[i]);
        b |= (!sylvan::Bdd::bddVar(resource_vars[i])) & (!sylvan::Bdd::bddVar(resource_vars_post[i]));
        rule &= b;
    }

    router.transitions |= rule;

    cout << "DONE router\n";

//////////////////////////////////////////////////////////

    std::vector<thread *> thread_list;

    thread_list.push_back(&generator);
    thread_list.push_back(&user);
    thread_list.push_back(&delay);
    thread_list.push_back(&sender);
    thread_list.push_back(&router);


    std::set<uint32_t> all_variables;
    sylvan::Bdd controller;

    /* variables on which the transitions depend but are abstracted out in the product */
    std::vector<uint32_t> other_variables = input_action_vars;
    for (size_t i = 0; i < output_action_vars.size(); i++) {
        other_variables.push_back(output_action_vars[i]);
    }

    cout << "ARENA BUILDING\n";

    /* Build the arena */
    std::map<std::string, sylvan::Bdd> progress_pred_to_bdd; /* this is a map that will be used in the rabin automaton for the specification */
    Arena A(all_variables,
            thread_list,
            resource_vars,
            resource_states,
            resource_vars_post,
            input_action_vars,
            output_action_vars,
            input_action_states,
            output_action_states,
            progress_pred_to_bdd);
    RabinAutomaton R(all_variables,
//                     A.nodes_,
                     progress_pred_to_bdd,
                     "../examples/lego/rabin_5_hoa.txt");

    cout << "ARENA DONE\n";

    FixedPoint fp(&A, &R, other_variables);


    controller = fp.Rabin(accl_on, M, A.nodes_, verbose);

    sylvan::sylvan_stats_report(stdout);
    sylvan::sylvan_quit();
    lace_stop();

    return 1;

}
