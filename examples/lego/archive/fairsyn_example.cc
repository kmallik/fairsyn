/* fairsyn.cc */

#include "Arena.hh"
#include "Fixpoint.cpp"
#include "RabinAutomaton.hh"
#include "TicToc.hh"
#include "testlib.h"

#include <chrono>
#include <random>
#include <sstream>
#include <fstream>
#include <iostream>
#include <set>
#include <utility> // for the class called pair
#include <algorithm> // for random shuffle

using namespace synthesis;

std::map<std::vector<int>, std::vector<std::vector<int> > > m;

std::map<std::vector<int>, std::vector<std::vector<int> > > m_final;

std::map<std::vector<int>, std::vector<std::vector<int> > > m_product;

std::map<std::vector<int>, std::vector<std::vector<int> > > live_edges;

std::map<std::vector<int>, int> map_states_to_nodes;

std::map<int, std::vector<int> > inverse_map_states_to_nodes;

std::vector<size_t> final_nodes;

std::vector<std::vector<size_t> > final_edges;

std::vector<std::vector<size_t> > final_live_edges;

std::map<std::vector<int>, bool> vis, vis1;

std::set<int> progress[10], ready[10];

void take_product(int cur_state, std::vector<int> state, int num_of_threads) {

    //cout<<vis1.size()<<"\n";

    std::vector<int> new_state = state;

    new_state.push_back(cur_state);

    if (vis1[new_state])
        return;


    vis1[new_state] = true;

    if (cur_state == num_of_threads - 1) {

        std::vector<int> next_state = new_state;
        next_state.pop_back();
        next_state.push_back(-1);
        m_product[new_state].push_back(next_state);
        take_product(-1, state, num_of_threads);
        return;

    }


    std::vector<std::vector<int> > sink = m_final[state];

    for (int i = 0; i < sink.size(); i++) {

        bool done = false;

        if (sink[i].size() == 1) {

            for (int j = 0; j < num_of_threads; j++) {

                if (progress[j].count(sink[i][0])) {

                    if (j - 1 == cur_state) {

                        done = true;

                        std::vector<int> next_state = sink[i];
                        next_state.push_back(j);
                        m_product[new_state].push_back(next_state);
                        take_product(j, sink[i], num_of_threads);


                    }

                }

            }

        }

        if (!done) {

            std::vector<int> next_state = sink[i];
            next_state.push_back(cur_state);
            m_product[new_state].push_back(next_state);
            take_product(cur_state, sink[i], num_of_threads);
        }
    }

}

void produce_all_states(std::vector<std::vector<int> > &nodes,
                        std::vector<std::vector<std::vector<std::vector<int> > > > &edges, std::vector<int> state,
                        int num_of_resources) {

    cout << vis.size() << "\n";

    vis[state] = true;

    // for(int i=0;i<state.size();i++){
    //     cout<<state[i]<<" ";
    // }
    // cout<<"\n";

    // this_thread::sleep_for(std::chrono::milliseconds(3000));

    if (state.back() == 0) {

        // cout<<"system_node\n";
        // for(int i=0;i<state.size();i++){
        //     cout<<state[i]<<" ";
        // }
        // cout<<"\n";


        bool add = false;

        for (int i = 0; i < nodes.size(); i++) {

            for (int j = 0; j < edges[i][state[i]].size(); j++) {


                int action = edges[i][state[i]][j][0];

                int sink_node = edges[i][state[i]][j][1];

                int resource_ind = (action + 2) / 3;

                int resource_act;

                if (action % 3 == 1)
                    resource_act = 0;
                else if (action % 3 == 2)
                    resource_act = -1;
                else
                    resource_act = 1;


                //cout<<action<<" "<<sink_node<<" " <<resource_act<<"\n";

                if (action % 3 == 0 || action % 3 == 1) {

                    //cout<<"add "<<action<<" "<<sink_node<<" " <<resource_act<<"\n";

                    if ((state[nodes.size() + resource_ind - 1] + resource_act == 1) ||
                        (state[nodes.size() + resource_ind - 1] + resource_act == 0)) {

                        add = true;

                    }

                } else {

                    std::vector<int> new_state = state;

                    new_state[i] = sink_node;

                    if (state[nodes.size() + resource_ind - 1] + resource_act == 0) {

                        new_state[nodes.size() + resource_ind - 1] += resource_act;

                        m[state].push_back(new_state);

                        if (!vis[new_state]) {

                            produce_all_states(nodes, edges, new_state, num_of_resources);

                        }

                    }


                }


            }

        }

        if (add) {

            std::vector<int> new_state = state;
            new_state[new_state.size() - 1] = 1;
            m[state].push_back(new_state);

            if (!vis[new_state]) {

                produce_all_states(nodes, edges, new_state, num_of_resources);

            }

        }


    } else {

        if (state[state.size() - 2] == -1) {

            for (int i = 0; i < nodes.size(); i++) {

                bool add = false;

                for (int j = 0; j < edges[i][state[i]].size(); j++) {

                    int action = edges[i][state[i]][j][0];

                    int sink_node = edges[i][state[i]][j][1];

                    int resource_ind = (action + 2) / 3;

                    int resource_act;

                    if (action % 3 == 1)
                        resource_act = 0;
                    else if (action % 3 == 2)
                        resource_act = -1;
                    else
                        resource_act = 1;

                    if (action % 3 == 0 || action % 3 == 1) {
                        if ((state[nodes.size() + resource_ind - 1] + resource_act == 1) ||
                            (state[nodes.size() + resource_ind - 1] + resource_act == 0)) {

                            add = true;

                        }
                    }
                }

                if (add) {

                    std::vector<int> new_state = state;
                    new_state[new_state.size() - 2] = i;
                    m[state].push_back(new_state);

                    if (!vis[new_state]) {

                        produce_all_states(nodes, edges, new_state, num_of_resources);

                    }

                }

            }


        } else {

            int i = state[state.size() - 2];

            for (int j = 0; j < edges[i][state[i]].size(); j++) {

                int action = edges[i][state[i]][j][0];

                int sink_node = edges[i][state[i]][j][1];

                int resource_ind = (action + 2) / 3;

                int resource_act;

                if (action % 3 == 1)
                    resource_act = 0;
                else if (action % 3 == 2)
                    resource_act = -1;
                else
                    resource_act = 1;

                if (action % 3 == 0 || action % 3 == 1) {
                    if ((state[nodes.size() + resource_ind - 1] + resource_act == 1) ||
                        (state[nodes.size() + resource_ind - 1] + resource_act == 0)) {

                        std::vector<int> new_state = state;
                        new_state[i] = sink_node;
                        new_state[new_state.size() - 1] = 0;
                        new_state[new_state.size() - 2] = -1;
                        new_state[nodes.size() + resource_ind - 1] += resource_act;
                        m[state].push_back(new_state);

                        if (!vis[new_state]) {

                            produce_all_states(nodes, edges, new_state, num_of_resources);

                        }

                    }
                }

            }

        }

    }

}

int str2int(string temp) {
    string s = temp;
    if (s[0] == '-') {
        return str2int(s.substr(1)) * -1;
    }
    std::stringstream ss(s);
    int x = 0;
    ss >> x;
    return x;
}


/*command line input includes
1.)file name ,
 2.)an upper bound on the number of rabin pairs ,
 3.)a number using which we divide the number of nodes
to get the upper bound on the number of vertices in a rabin condition
ie (1/x) * N where x is the command line input and N is the number of nodes
4.) another same for the live edges
*/

int main(int argc, char *argv[]) {

    int example = str2int(argv[1]);


    /* initiate the lace work-stealing framework and the sylvan parallel bdd library */
    int n_workers = 0; // 0 for auto-detect
    int dqsize = 1000000;
    lace_start(n_workers, dqsize);
    // use at most 1 GB, nodes:cache ratio 2:1, initial size 1/32 of maximum
    sylvan::sylvan_set_limits(1 * 1024 * 1024 * 1024, 1, 5);
    sylvan::sylvan_init_package();
    sylvan::sylvan_init_mtbdd();

//    sylvan::Bdd a = sylvan::Bdd::bddVar(0);
//    std::cout << "index of a = " << a.TopVar() << "\n";
//    sylvan::Bdd b = sylvan::Bdd::bddVar(0);
//    std::cout << "index of b = " << b.TopVar() << "\n";
//    a = sylvan::Bdd::bddZero();
//    b = sylvan::Bdd::bddZero();
//    assert(a==b);
//    sylvan::Bdd c = a|b;
//    std::cout << "number of nodes in a= " << a.NodeCount() << "\n";
//    std::cout << "number of nodes in b= " << b.NodeCount() << "\n";



// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

    //  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    std::vector<size_t> sys_nodes;
    std::vector<size_t> env_nodes;

    // 1 -- resource request a
    // 2 -- resource grant a
    // 3 -- resource release a
    // 4 -- resource request b
    // 5 -- resource grant b
    // 6 -- resource release b
    // 7 -- resource request c
    // 8 -- resource grant c
    // 9 -- resource release c

    std::vector<std::vector<int> > nodes;
    std::vector<std::vector<std::vector<std::vector<int> > > > edges;

    if (example == 1) {


        nodes = {
                {0, 1, 2, 3, 4, 5, 6},//generator
                // { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //user
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //delay
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33}, //router
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} //sender
        };

        edges = {
                {
                        {{1,  1}},
                        {{2,  5}},
                        {{7,  3}},
                        {{8,  4}},
                        {{9,  5}},
                        {{6,  0}},
                },
                // {
                // { { 13 , 1} },
                // { { 14 , 2} },
                // { { 1 , 3} },
                // { { 2 , 10} },
                // { { 16 , 5} },
                // { { 17 , 6} },
                // { { 7 , 7} },
                // { { 8 , 8} },
                // { { 18 , 9} },
                // { { 9 , 10} },
                // { { 6 , 11} },
                // { { 12 , 0} }
                // },
                {
                        {{22, 1}},
                        {{23, 2}},
                        {{28, 3}},
                        {{29, 10}},
                        {{25, 5}},
                        {{26, 6}},
                        {{34, 7}},
                        {{35, 8}},
                        {{27, 9}},
                        {{36, 10}},
                        {{33, 11}},
                        {{21, 0}}
                },
                {
                        {{4,  12}, {4,  23}},
                        {{5,  2}},
                        {{10, 3}},
                        {{11, 10}},
                        {{7,  5}},
                        {{8,  6}},
                        {{16, 7}},
                        {{17, 8}},
                        {{9,  9}},
                        {{18, 10}},
                        {{15, 11}},
                        {{3,  0}},
                        {{5,  13}},
                        {{19, 14}},
                        {{20, 21}},
                        {{7,  16}},
                        {{8,  17}},
                        {{25, 18}},
                        {{26, 19}},
                        {{9, 20}},
                        {{27, 21}},
                        {{24, 22}},
                        {{3, 0}},//
                        {{5, 24}},
                        {{28, 25}},
                        {{29, 32}},
                        {{7, 27}},
                        {{8, 28}},
                        {{34, 29}},
                        {{35, 30}},
                        {{9, 31}},
                        {{36, 32}},
                        {{33, 33}},
                        {{3, 0}}
                },
                {
                        {{31, 1},  {31, 6}},
                        {{32, 2}},
                        {{34, 3}},
                        {{35, 4}},
                        {{36, 5}},
                        {{30, 0}},
                        {{32, 7}},
                        {{19, 8}},
                        {{20, 9}},
                        {{34, 10}},
                        {{35, 11}},
                        {{25, 12}},
                        {{26, 13}},
                        {{36, 14}},
                        {{27, 15}},
                        {{24, 16}},
                        {{30, 0}}
                }
        };

    } else if (example == 2) {
        nodes = {
                {0, 1, 2, 3, 4, 5, 6},//generator
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //user
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //delay
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33}, //router
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} //sender
        };

        edges = {
                {
                        {{1,  1}},
                        {{2,  5}},
                        {{7,  3}},
                        {{8,  4}},
                        {{9,  5}},
                        {{6,  0}},
                },
                {
                        {{13, 1}},
                        {{14, 2}},
                        {{1,  3}},
                        {{2,  10}},
                        {{16, 5}},
                        {{17, 6}},
                        {{7,  7}},
                        {{8,  8}},
                        {{18, 9}},
                        {{9,  10}},
                        {{6,  11}},
                        {{12, 0}}
                },
                {
                        {{22, 1}},
                        {{23, 2}},
                        {{28, 3}},
                        {{29, 10}},
                        {{25, 5}},
                        {{26, 6}},
                        {{34, 7}},
                        {{35, 8}},
                        {{27, 9}},
                        {{36, 10}},
                        {{33, 11}},
                        {{21, 0}}
                },
                {
                        {{4,  1}, {4,  12}, {4, 23}},
                        {{5,  2}},
                        {{10, 3}},
                        {{11, 10}},
                        {{7,  5}},
                        {{8,  6}},
                        {{16, 7}},
                        {{17, 8}},
                        {{9,  9}},
                        {{18, 10}},
                        {{15, 11}},
                        {{3,  0}},
                        {{5,  13}},
                        {{19, 14}},
                        {{20, 21}},
                        {{7,  16}},
                        {{8,  17}},
                        {{25, 18}},
                        {{26, 19}},
                        {{9, 20}},
                        {{27, 21}},
                        {{24, 22}},
                        {{3, 0}},//
                        {{5, 24}},
                        {{28, 25}},
                        {{29, 32}},
                        {{7, 27}},
                        {{8, 28}},
                        {{34, 29}},
                        {{35, 30}},
                        {{9, 31}},
                        {{36, 32}},
                        {{33, 33}},
                        {{3, 0}}
                },
                {
                        {{31, 1}, {31, 6}},
                        {{32, 2}},
                        {{34, 3}},
                        {{35, 4}},
                        {{36, 5}},
                        {{30, 0}},
                        {{32, 7}},
                        {{19, 8}},
                        {{20, 9}},
                        {{34, 10}},
                        {{35, 11}},
                        {{25, 12}},
                        {{26, 13}},
                        {{36, 14}},
                        {{27, 15}},
                        {{24, 16}},
                        {{30, 0}}
                }
        };
    } else if (example == 3) {

        nodes = {
                {0, 1, 2, 3, 4, 5, 6},//generator
                //{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //user
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //delay
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33}, //router
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} //sender
        };

        edges = {
                {
                        {{1,  1}},
                        {{2,  2}},
                        {{7,  3}},
                        {{8,  4}},
                        {{9,  5}},
                        {{6,  0}},
                },
                // {
                // { { 13 , 1} },
                // { { 14 , 2} },
                // { { 1 , 3} },
                // { { 2 , 4} },
                // { { 16 , 5} },
                // { { 17 , 6} },
                // { { 7 , 7} },
                // { { 8 , 8} },
                // { { 18 , 9} },
                // { { 9 , 10} },
                // { { 6 , 11} },
                // { { 12 , 0} }
                // },
                {
                        {{22, 1}},
                        {{23, 2}},
                        {{28, 3}},
                        {{29, 4}},
                        {{25, 5}},
                        {{26, 6}},
                        {{34, 7}},
                        {{35, 8}},
                        {{27, 9}},
                        {{36, 10}},
                        {{33, 11}},
                        {{21, 0}}
                },
                {
                        {{4,  12}, {4,  23}},
                        {{5,  2}},
                        {{10, 3}},
                        {{11, 4}},
                        {{7,  5}},
                        {{8,  6}},
                        {{16, 7}},
                        {{17, 8}},
                        {{9,  9}},
                        {{18, 10}},
                        {{15, 11}},
                        {{3,  0}},
                        {{5,  13}},
                        {{19, 14}},
                        {{20, 15}},
                        {{7,  16}},
                        {{8,  17}},
                        {{25, 18}},
                        {{26, 19}},
                        {{9, 20}},
                        {{27, 21}},
                        {{24, 22}},
                        {{3, 0}},//
                        {{5, 24}},
                        {{28, 25}},
                        {{29, 26}},
                        {{7, 27}},
                        {{8, 28}},
                        {{34, 29}},
                        {{35, 30}},
                        {{9, 31}},
                        {{36, 32}},
                        {{33, 33}},
                        {{3, 0}}
                },
                {
                        {{31, 1},  {31, 6}},
                        {{32, 2}},
                        {{34, 3}},
                        {{35, 4}},
                        {{36, 5}},
                        {{30, 0}},
                        {{32, 7}},
                        {{19, 8}},
                        {{20, 9}},
                        {{34, 10}},
                        {{35, 11}},
                        {{25, 12}},
                        {{26, 13}},
                        {{36, 14}},
                        {{27, 15}},
                        {{24, 16}},
                        {{30, 0}}
                }
        };
    } else {
        nodes = {
                {0, 1, 2, 3, 4, 5, 6},//generator
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //user
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, //delay
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33}, //router
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} //sender
        };

        edges = {
                {
                        {{1,  1}},
                        {{2,  2}},
                        {{7,  3}},
                        {{8,  4}},
                        {{9,  5}},
                        {{6,  0}},
                },
                {
                        {{13, 1}},
                        {{14, 2}},
                        {{1,  3}},
                        {{2,  4}},
                        {{16, 5}},
                        {{17, 6}},
                        {{7,  7}},
                        {{8,  8}},
                        {{18, 9}},
                        {{9,  10}},
                        {{6,  11}},
                        {{12, 0}}
                },
                {
                        {{22, 1}},
                        {{23, 2}},
                        {{28, 3}},
                        {{29, 4}},
                        {{25, 5}},
                        {{26, 6}},
                        {{34, 7}},
                        {{35, 8}},
                        {{27, 9}},
                        {{36, 10}},
                        {{33, 11}},
                        {{21, 0}}
                },
                {
                        {{4,  1}, {4,  12}, {4, 23}},
                        {{5,  2}},
                        {{10, 3}},
                        {{11, 4}},
                        {{7,  5}},
                        {{8,  6}},
                        {{16, 7}},
                        {{17, 8}},
                        {{9,  9}},
                        {{18, 10}},
                        {{15, 11}},
                        {{3,  0}},
                        {{5,  13}},
                        {{19, 14}},
                        {{20, 15}},
                        {{7,  16}},
                        {{8,  17}},
                        {{25, 18}},
                        {{26, 19}},
                        {{9, 20}},
                        {{27, 21}},
                        {{24, 22}},
                        {{3, 0}},//
                        {{5, 24}},
                        {{28, 25}},
                        {{29, 26}},
                        {{7, 27}},
                        {{8, 28}},
                        {{34, 29}},
                        {{35, 30}},
                        {{9, 31}},
                        {{36, 32}},
                        {{33, 33}},
                        {{3, 0}}
                },
                {
                        {{31, 1}, {31, 6}},
                        {{32, 2}},
                        {{34, 3}},
                        {{35, 4}},
                        {{36, 5}},
                        {{30, 0}},
                        {{32, 7}},
                        {{19, 8}},
                        {{20, 9}},
                        {{34, 10}},
                        {{35, 11}},
                        {{25, 12}},
                        {{26, 13}},
                        {{36, 14}},
                        {{27, 15}},
                        {{24, 16}},
                        {{30, 0}}
                }
        };
    }


    int number_of_mutex = 4;
    int number_of_semaphore = 8;
    int number_of_queue = 4;

    std::vector<int> temp;

    for (int i = 0; i < nodes.size(); i++) {
        temp.push_back(nodes[i][0]);
    }

    for (int i = 0; i < number_of_queue; i++) {
        temp.push_back(1);
        temp.push_back(0);
        temp.push_back(1);
    }

    temp.push_back(-1);
    temp.push_back(0);

    cout << "PRODUCING PRODUCT -------- \n";

    produce_all_states(nodes, edges, temp, number_of_queue);

    std::map<std::vector<int>, std::vector<std::vector<int> > >::iterator it;


    // for( it = m.begin(); it != m.end(); it++){

    //     std::vector<int> source =it->first;

    //     std::vector<std::vector<int> > sink = it->second;

    //     cout<<"vertex\n";
    //     for(int i=0;i<source.size();i++){
    //         cout<<source[i]<<" ";
    //     }
    //     cout<<"\n";
    //     cout<<"connected vertices\n";
    //     for(int i=0;i<sink.size();i++){
    //         for(int j=0;j<sink[i].size();j++){
    //             cout<<sink[i][j]<<" ";
    //         }
    //         cout<<"\n";
    //     }
    // }

    int cnt = 1e3;


    for (it = m.begin(); it != m.end(); it++) {

        std::vector<int> source = it->first;

        std::vector<std::vector<int> > sink = it->second;

        std::vector<std::vector<int> > temp;

        // cout<<"source_node\n";

        // for(int i=0;i<source.size();i++){
        //         cout<<source[i]<<" ";
        // }

        // cout<<"\n";

        for (int i = 0; i < sink.size(); i++) {

            if (source[source.size() - 1] == 0 && sink[i][sink[i].size() - 1] == 1) {
                temp.push_back(sink[i]);
            } else if (source[source.size() - 1] == 1 && sink[i][sink[i].size() - 1] == 1 &&
                       source[source.size() - 2] == -1) {
                temp.push_back(sink[i]);
            } else {

                ++cnt;
                std::vector<int> store;
                store.push_back(cnt);
                temp.push_back(store);
                m_final[store].push_back(sink[i]);

                for (int j = 0; j < nodes.size(); j++) {
                    if (source[j] != sink[i][j]) {
                        progress[j].insert(cnt);
                    }
                }

            }

        }

        m_final[source] = temp;
    }

    //cout<<"hi\n";

    // std::map<std::vector<int> , std::vector<std::vector<int> > > :: iterator it;

    // for( it = m.begin(); it != m.end(); it++){

    //     std::vector<int> source =it->first;

    //     std::vector<std::vector<int> > sink = it->second;

    //     cout<<"vertex\n";
    //     for(int i=0;i<source.size();i++){
    //         cout<<source[i]<<" ";
    //     }
    //     cout<<"\n";
    //     cout<<"connected vertices\n";
    //     for(int i=0;i<sink.size();i++){
    //         for(int j=0;j<sink[i].size();j++){
    //             cout<<sink[i][j]<<" ";
    //         }
    //         cout<<"\n";
    //     }
    // }


    // for( it = m_final.begin(); it != m_final.end(); it++){

    //     std::vector<int> source =it->first;

    //     std::vector<std::vector<int> > sink = it->second;

    //     cout<<"vertex\n";
    //     for(int i=0;i<source.size();i++){
    //         cout<<source[i]<<" ";
    //     }
    //     cout<<"\n";
    //     cout<<"connected vertices\n";
    //     for(int i=0;i<sink.size();i++){
    //         for(int j=0;j<sink[i].size();j++){
    //             cout<<sink[i][j]<<" ";
    //         }
    //         cout<<"\n";
    //     }

    // }

    cout << "PRODUCING FINAL STATES  -------- \n";

    take_product(-1, temp, nodes.size());


    // for( it = m_product.begin(); it != m_product.end(); it++){

    //     std::vector<int> source =it->first;

    //     std::vector<std::vector<int> > sink = it->second;

    //     cout<<"vertex\n";
    //     for(int i=0;i<source.size();i++){
    //         cout<<source[i]<<" ";
    //     }
    //     cout<<"\n";
    //     cout<<"connected vertices\n";
    //     for(int i=0;i<sink.size();i++){
    //         for(int j=0;j<sink[i].size();j++){
    //             cout<<sink[i][j]<<" ";
    //         }
    //         cout<<"\n";
    //     }
    // }

    for (it = m_product.begin(); it != m_product.end(); it++) {

        std::vector<int> source = it->first;

        std::vector<std::vector<int> > sink = it->second;

        if (source.size() == 2)
            continue;

        int len = source.size();
        if (source[len - 2] == 1 && source[len - 3] == -1) {
            live_edges[source] = m_product[source];
        }

    }

    for (it = m_product.begin(); it != m_product.end(); it++) {

        std::vector<int> source = it->first;

        std::vector<std::vector<int> > sink = it->second;

        if (source.size() == 2)
            continue;

        int len = source.size();
        if (source[len - 3] == 0 && source[0] == 0) {
            live_edges[source] = m_product[source];
        }

    }


    for (it = m_product.begin(); it != m_product.end(); it++) {

        std::vector<int> source = it->first;

        std::vector<std::vector<int> > sink = it->second;

        if (source.size() == 2)
            continue;

        int len = source.size();
        if (source[len - 3] == 3 && source[3] == 0) {
            live_edges[source] = m_product[source];
        }

    }


    cnt = 0;

    for (it = m_product.begin(); it != m_product.end(); it++) {

        std::vector<int> source = it->first;

        map_states_to_nodes[source] = cnt;
        final_nodes.push_back(cnt);
        if (source.size() == 2 || source[source.size() - 2] == 1) {
            env_nodes.push_back(cnt);
        } else {
            sys_nodes.push_back(cnt);
        }

        inverse_map_states_to_nodes[cnt++] = source;

    }

    for (int i = 0; i < cnt; i++) {

        std::vector<int> state = inverse_map_states_to_nodes[i];

        std::vector<std::vector<int> > sink = m_product[state];

        std::vector<size_t> temp;

        std::vector<size_t> emp;

        for (int j = 0; j < sink.size(); j++) {

            temp.push_back(map_states_to_nodes[sink[j]]);

        }

        final_edges.push_back(temp);

        final_live_edges.push_back(emp);

    }

    for (it = live_edges.begin(); it != live_edges.end(); it++) {

        std::vector<int> source = it->first;

        int ind = map_states_to_nodes[source];

        final_live_edges[ind] = final_edges[ind];

    }

    cout << "DONE PRODUCTION\n";

    std::vector<size_t> G;
    std::vector<size_t> R;

    for (int i = 0; i < cnt; i++) {
        std::vector<int> state = inverse_map_states_to_nodes[i];
        if (state.back() == 1) {
            G.push_back(i);
        }
    }


    std::array<std::vector<size_t>, 2> p = {G, R};
    std::vector<std::array<std::vector<size_t>, 2>> RabinPairs = {p};


    /* starting the faster algo */

    std::set<uint32_t> all_variables;
    sylvan::Bdd controller_strategy;

    FixedPoint Fp(final_nodes, sys_nodes, env_nodes, final_edges, final_live_edges, RabinPairs, all_variables);
    //std::cout<<"hi fixed point done\n";


    controller_strategy = Fp.Rabin(true, 10, Fp.nodes_, 1);

    /* cout<<"Final Output\n\n";
     uint32_t nvars=Fp.preVars_.size();
     sylvan::BddSet preBddVars=sylvan::BddSet::fromVector(Fp.preVars_);
     sylvan::BddSet postBddVars=sylvan::BddSet::fromVector(Fp.postVars_);

     for (size_t i=0; i<transitions.size(); i++) {

             sylvan::Bdd cur_node=elementToBdd(i,preBddVars,nvars);

             for(int j=0;j<transitions[i].size();j++){
                 sylvan::Bdd temp = sylvan::Bdd::bddZero();
                 sylvan::Bdd succ_node=elementToBdd(transitions[i][j],postBddVars,nvars);
                 temp = (cur_node & succ_node);
                 sylvan::Bdd intermediate = temp & controller_strategy;
             if( temp == intermediate ){
                 cout<<transitions[i][j]<<" ";
             }
             }
         cout<<-1<<"\n";
     }*/


    uint32_t nvars = Fp.preVars_.size();
    sylvan::BddSet preBddVars = sylvan::BddSet::fromVector(Fp.preVars_);
    sylvan::BddSet postBddVars = sylvan::BddSet::fromVector(Fp.postVars_);

    cout << std::endl;
    std::cout << " Final Controller = \n";
    for (int i = 0; i < pow(2, Fp.preVars_.size()); i++) {
        cout << "new vertex\n";
        cout << i << " :: ";
        std::vector<int> state = inverse_map_states_to_nodes[i];
        for (int j = 0; j < state.size(); j++) {
            cout << state[j] << " ";
        }
        cout << "\n";
        sylvan::Bdd cur_node = elementToBdd(i, preBddVars, nvars);
        for (int j = 0; j < pow(2, Fp.preVars_.size()); j++) {
            sylvan::Bdd temp = sylvan::Bdd::bddZero();
            sylvan::Bdd succ_node = elementToBdd(j, postBddVars, nvars);
            temp = (cur_node & succ_node);
            sylvan::Bdd intermediate = temp & controller_strategy;
            if (temp == intermediate) {
                cout << "strategy vertex \n";
                cout << j << " :: ";
                state = inverse_map_states_to_nodes[j];
                for (int k = 0; k < state.size(); k++) {
                    cout << state[k] << " ";
                }
                cout << "\n";
            }
        }
        cout << "---------------------";
        cout << std::endl << "\n";
    }



    /* some sylvan statistics */

    sylvan::sylvan_stats_report(stdout);
    sylvan::sylvan_quit();
    lace_stop();

    return 1;

}

