/*! \file Helper.hh
 *  Contains helper functions.
 */

#ifndef HELPER_HH_
#define HELPER_HH_

#include <algorithm>
#include <cmath>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>

using std::clog;
using std::cout;
using std::string;

namespace helper {

    /*!
     * Change types from vector<size_t> to vector<uint32_t>
     */
    std::vector<uint32_t> to_uint32_t(std::vector<size_t> vec) {
        return std::vector<uint32_t>(vec.begin(), vec.end());
    }

    /*! Ensures that a specified subdirectory exists.
     *  \param[in]  dirName     Name of the desired subdirectory.
     */
    template <class dir_type>
    void checkMakeDir(dir_type dirName) {
        DIR *dir = opendir(dirName);
        if (dir) {
            closedir(dir);
        } else if (ENOENT == errno) {
            int result = mkdir(dirName, 0777);
            (void)result;
        }
    }

    /*! Prints the contents of an array to a log file.
     *  \param[in] array    Array.
     *  \param[in] size     Number of elements.
     */
    template <class array_type>
    void printArray(array_type array, int size) {
        for (int i = 0; i < size; i++) {
            clog << array[i] << ' ';
        }
        clog << '\n';
    }

    /*! Writes a vector of SymbolicSets into a subdirectory.
     *  \param[in] vec       Vector of SymbolicSets.
     *  \param[in] vecPrefix Subdirectory name + '/' + filename prefix.
     */
    template <class vec_type, class prefix_type>
    void saveVec(vec_type vec, prefix_type vecPrefix) {
        for (size_t i = 0; i < vec.size(); i++) {
            string Str = "";
            Str += vecPrefix;
            Str += std::to_string(i + 1);
            Str += ".bdd";
            char Char[20];
            size_t Length = Str.copy(Char, Str.length() + 1);
            Char[Length] = '\0';
            //            cout << Char << '\n';
            vec[i]->writeToFile(Char);
        }
    }

    /*! Frees a vector's objects from heap memory.
     *  \param[in]  vec     Vector of objects.
     */
    template <class vec_type>
    void deleteVec(vec_type vec) {
        for (size_t i = 0; i < vec.size(); i++) {
            delete vec[i];
        }
    }

    template <class vecVec_type>
    void deleteVecVec(vecVec_type vecVec) {
        for (size_t i = 0; i < vecVec.size(); i++) {
            deleteVec(*vecVec[i]);
            delete vecVec[i];
        }
    }

    /*! Prints a vector's SymbolicSets to the console.
     *  \param[in]  vec         Vector of SymbolicSets.
     *  \param[in]  vecPrefix   Name of a SymbolicSet.
     */
    template <class vec_type, class prefix_type>
    void printVec(vec_type vec, prefix_type vecPrefix) {
        for (size_t i = 0; i < vec.size(); i++) {
            cout << vecPrefix << ' ' << i << ":\n";
            vec[i]->printInfo(1);
        }
    }

    template <class vec_t, class prefix_t>
    void printVecArray(vec_t vec, prefix_t prefix, int arrayElems) {
        for (size_t i = 0; i < vec.size(); i++) {
            clog << prefix << "[" << i << "]: ";
            printArray(vec[i], arrayElems);
        }
    }

    template <class vecVec_type, class prefix_type>
    void printVecVec(vecVec_type vecVec, prefix_type vecPrefix) {
        for (size_t i = 0; i < vecVec.size(); i++) {
            for (size_t j = 0; j < vecVec[i]->size(); j++) {
                cout << vecPrefix << '[' << i << "][" << j << "]:\n";
                (*vecVec[i])[j]->printInfo(1);
            }
        }
    }

    /*! Frees a vector's arrays from heap memory.
     *  \param[in]  vec     Vector of arrays.
     */
    template <class vec_type>
    void deleteVecArray(vec_type vec) {
        for (size_t i = 0; i < vec.size(); i++) {
            delete[] vec[i];
        }
    }

    template <class vecVec_type>
    void deleteVecVecArray(vecVec_type vecVec) {
        for (size_t i = 0; i < vecVec.size(); i++) {
            deleteVecArray(*vecVec[i]);
            delete vecVec[i];
        }
    }


    std::vector<uint8_t> get_bin(size_t x, size_t nvars) {
        std::vector<uint8_t> bin(nvars, 0);
        for (size_t p = 0; x; x /= 2, p++) bin[p] = 0 + x % 2;
        return bin;
    }

    template <class UBDD>
    std::vector<UBDD> toUBDD(const UBDD &base, const std::vector<size_t> &ubddVars) {
        std::vector<UBDD> ubdds(ubddVars.size());
        for (int i = 0; i < ubddVars.size(); i++) {
            ubdds[i] = base.var(ubddVars[i]);
        }
        return ubdds;
    }

    template <class UBDD>
    UBDD elementToBdd(const UBDD &base,
                      const size_t &x,
                      const std::vector<size_t> &ubddVars,
                      const size_t nvars) {
        return base.cube(toUBDD<UBDD>(base, ubddVars), get_bin(x, nvars));
    }

    template <class UBDD>
    UBDD setToBdd(const UBDD &base,
                  const std::vector<size_t> &set,
                  const std::vector<size_t> &ubddVars,
                  const size_t nvars) {
        UBDD set_bdd = base.zero();
        for (auto i = std::begin(set); i != std::end(set); ++i) {
            set_bdd |= elementToBdd<UBDD>(base, *i, ubddVars, nvars);
        }
        return set_bdd;
    }

    //store winning region in file
    template <class UBDD>
    void winning_region_file(const UBDD &base,
                             const UBDD &controller,
                             const std::vector<size_t> &preVars_,
                             const string &file_name,
                             std::vector<bool> output) {

        std::fstream newfile;
        newfile.open(file_name, std::fstream::out);

        uint32_t nvars = std::ceil(std::log2(output.size()));

        for (size_t i = 0; i < output.size(); i++) {
            if (!output[i])
                continue;

            UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, nvars);
            UBDD intermediate = cur_node & controller;

            if (intermediate != controller.zero()) {
                newfile << i << "\n";
            } else {
                newfile << -1 << "\n";
            }
        }

        newfile.close();
    }

    //store the conroller strategy for all system nodes
    template <class UBDD>
    void controller_file(const UBDD &base,
                         const UBDD &controller,
                         const std::vector<size_t> &preVars_,
                         const std::vector<size_t> &postVars_,
                         const std::vector<std::vector<size_t>> &transitions,
                         const string &file_name,
                         std::vector<bool> output) {

        std::fstream newfile;
        newfile.open(file_name, std::fstream::out);

        uint32_t nvars = std::ceil(std::log2(output.size()));

        for (size_t i = 0; i < output.size(); i++) {

            if (!output[i]) {

                newfile << -1 << "\n";
                continue;
            }

            UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, nvars);

            for (size_t j = 0; j < transitions[i].size(); j++) {

                UBDD temp = controller.zero();
                UBDD succ_node = elementToBdd<UBDD>(base, transitions[i][j], postVars_, nvars);
                temp = (cur_node & succ_node);
                UBDD intermediate = temp & controller;

                if (temp == intermediate) {
                    newfile << transitions[i][j] << " ";
                }
            }

            newfile << -1 << "\n";
        }

        newfile.close();
    }
    template <class UBDD>
    void store_bdd_in_file(const UBDD &base, const UBDD &store,
                           const std::vector<size_t> &preVars_,
                           const std::vector<size_t> &postVars_,
                           const string &file_name) {
        std::fstream newfile;
        newfile.open(file_name, std::fstream::out);
        for (size_t i = 0; i < pow(2, preVars_.size()); i++) {
            newfile << i << " -> ";
            for (size_t j = 0; j < pow(2, postVars_.size()); j++) {
                UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, preVars_.size());
                UBDD next_node = elementToBdd<UBDD>(base, j, postVars_, postVars_.size());
                UBDD intermediate = (cur_node & next_node) & store;
                if (intermediate != store.zero()) {
                    newfile << j << " ";
                }
            }
            newfile << -1 << "\n";
        }
        newfile.close();
    }

    template <class UBDD>
    void print_bdd_info(const UBDD &base, const UBDD &store,
                        const std::vector<uint32_t> &preVars_,
                        const std::vector<uint32_t> &postVars_) {

        uint32_t nvars = preVars_.size();

        cout << std::endl;
        std::cout << "\t Asked to print states = ";
        for (int i = 0; i < pow(2, preVars_.size()); i++) {
            cout << i << " ";
            UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, nvars);
            for (int j = 0; j < pow(2, preVars_.size()); j++) {
                UBDD temp = store.zero();
                UBDD succ_node = elementToBdd<UBDD>(base, j, postVars_, nvars);
                temp = (cur_node & succ_node);
                UBDD intermediate = temp & store;

                if (intermediate != store.zero()) {
                    cout << " " << j << " ";
                }
            }
            cout << -1;
            cout << std::endl
                 << "\n";
        }
    }

    // todo in the future everything should be size_t instead of uint32_t
    template <class UBDD>
    void print_bdd_info(const UBDD &base, const UBDD &store,
                        const std::vector<size_t> &preVars_,
                        const std::vector<size_t> &postVars_) {
        print_bdd_info<UBDD>(base, store, to_uint32_t(preVars_), to_uint32_t(postVars_));
    }

    template <class UBDD>
    void
    print_bdd_info(const UBDD &base, const UBDD &store, const std::vector<uint32_t> &preVars_,
                   const std::vector<uint32_t> &inpVars_,
                   const std::vector<uint32_t> &postVars_) {

        uint32_t nvars = preVars_.size();

        cout << std::endl;
        std::cout << "\t Asked to print transitions = \n";
        for (int i = 0; i < pow(2, preVars_.size()); i++) {
            for (size_t k = 0; k < pow(2, inpVars_.size()); k++) {
                cout << "(" << i << ", " << k << "): ";
                UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, nvars);
                UBDD cur_input = elementToBdd<UBDD>(base, k, inpVars_, inpVars_.size());
                for (int j = 0; j < pow(2, postVars_.size()); j++) {
                    UBDD temp = base.zero();
                    UBDD succ_node = elementToBdd<UBDD>(base, j, postVars_, postVars_.size());
                    temp = (cur_node & cur_input & succ_node);
                    UBDD intermediate = temp & store;
                    // if( temp == intermediate ){
                    if (intermediate != base.zero()) {
                        cout << " " << j << " ";
                    }
                }
                cout << "\n";
            }

            cout << -1;
            cout << std::endl
                 << "\n";
        }
    }
    template <class UBDD>
    void print_bdd_info(const UBDD &base, const UBDD &store, const std::vector<uint32_t> &preVars_) {

        uint32_t nvars = preVars_.size();

        for (int i = 0; i < pow(2, preVars_.size()); i++) {
            UBDD cur_node = elementToBdd<UBDD>(base, i, preVars_, nvars);
            UBDD intermediate = cur_node & store;
            if (intermediate != base.zero()) {
                cout << i << " ";
            }
        }
        cout << -1 << " ";
    }

    template <class UBDD>
    void print_succ(const UBDD &base, const UBDD tr, const size_t node_id, const std::vector<uint32_t> preVars_,
                    const std::vector<uint32_t> postVars_) {

        uint32_t nvars = preVars_.size();
        UBDD cur_node = elementToBdd<UBDD>(base, node_id, preVars_, nvars);
        // UBDD succ_nodes=cur_node&tr;
        UBDD cubePre = base.cube(toUBDD(base, preVars_));
        UBDD succ_nodes = tr.andAbstract(cur_node, cubePre);

        print_bdd_info(succ_nodes, postVars_);
    }

    template <class UBDD>
    inline size_t get_nof_elements(const UBDD &set, const size_t nvars, const UBDD &cube) {
        return set.existAbstract(cube).countMinterm(nvars);
    }

    template <class UBDD>
    int is_element(const UBDD &base, const UBDD &set, const size_t node_id, const std::vector<uint32_t> &vars) {
        UBDD element = elementToBdd<UBDD>(base, node_id, vars, vars.size());
        if (element <= set)
            return 1;
        else
            return 0;
    }

    /* function: sort */
    template <class T>
    inline std::vector<T> sort(const std::vector<T> &vec) {
        std::vector<T> sorted = vec;
        std::sort(sorted.begin(), sorted.end());
        return sorted;
    }

}// namespace helper

#endif /* HELPER_HH_ */
