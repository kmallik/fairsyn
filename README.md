# Fairsyn

Parallel reactive synthesis for omega-regular specifications under the transition fairness condition

# Install

To use this project you must download the [Genie](https://gitlab.mpi-sws.org/mrychlicki/genie) repository
and then compile it according to the instructions. Then run:

```
mkdir build
cd build
cmake .. -Dgenie_HOME=/path/to/genie/ -DCMAKE_PREFIX_PATH=/path/to/libraries/
make
cd ..
```

Note: the paths should be specified as aboslute paths. The flag `-DCMAKE_PREFIX_PATH` is needed when the libraries (cudd, sylvan, hoaf) are installed in location other than `/usr/local/`.

Now this project is compiled and can be used.

## Documantation

To create documentation run:

```
cd docs
export genie_HOME=/path/to/genie/
doxygen Doxyfile.in
```

and now documentation can be found in `docs/html/index.html`.

[//]: # (from commit b88160a8)
CAV '23 Artifact
==================
A draft version of our CAV '23 can be obtained from this
[link](https://mascot.mpi-sws.org/papers/cav23_toolpaper.pdf),
while the artefact with virtual machine can be found at this 
[link](https://zenodo.org/record/7877791#.ZExTJZFBxH4). 

TACAS '22 Artifact
==================

You can obtain the draft of our TACAS '22 submission along with the artifact in this [link](https://calendar.mpi-sws.org/index.php/s/ALDTtcJHZRezpq6). We tested the artifact on the [virtual machine](https://zenodo.org/record/5562597#.YYULqSUo_mE) provided by the TACAS '22 AE Committee. To generate the results, you need to simply extract the TACAS_AE_Archive.zip file within the virtual machine, and then follow the instructions provided in the Readme.txt file.
